package org.usfirst.frc.team702.robot;

import org.usfirst.frc.team702.robot.AutoDriver.SensorDirection;
import org.usfirst.frc.team702.robot.GearDelivery.GearState;
import org.usfirst.frc.team702.sensors.PixyCmu5;
import org.usfirst.frc.team702.tools.OneShotBoolean;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Autonomous {

	// autonomous variables
	public static enum AutonomousDriveStep{
		NONE,
		DRIVE_MIDDLE,
		DRIVE_RIGHT,
		DRIVE_LEFT
	}
	
	// autonomous variables
	public static enum AutonomousShootStep{
		NONE,
		SHOOT_LEFT,
		SHOOT_RIGHT
	}
	
	//Possible steps in an auto sequence
	public static enum autonomousStep {
		START,
		DRIVE,
		DELIVER,
		LINE_UP,
		STOP,
		FIND_TARGET,
		TURN_TO_ANGLE,
		APPROACH,
		ALIGN,
		REVERSE,
		PREPARETOSHOOT,
		SHOOT,
		DRIVEBLIND,
		LOWERGEAR,
		RAISEGEAR
	}
	
	private Robot m_robot;
	private autonomousStep nextState = autonomousStep.START;
	private autonomousStep currentState = autonomousStep.START;
	
	private Timer autoTimer;
	
	private AutonomousDriveStep selectedAutonomousDrive = AutonomousDriveStep.DRIVE_MIDDLE;
	private AutonomousShootStep selectedAutonomousShoot = AutonomousShootStep.NONE;
	
	private OneShotBoolean successBoolean;
	
	private int timesInState;
	
	private Timer timer;
	
	
	/**
	 * Autonomous mode constructor
	 * 
	 * @param Robot class
	 */
	public Autonomous(Robot robot)
	{
		m_robot = robot;
		
		autoTimer = new Timer();
		autoTimer.reset();
	
		successBoolean = new OneShotBoolean(10);
		timesInState = 0;
		
		timer = new Timer();
		
		SmartDashboard.putString("Selected Autonomous", selectedAutonomousDrive.toString());
		SmartDashboard.putString("Autonomous State", "");
	}
	
	public void Start()
	{
		m_robot.m_imu.zeroYaw();
		autoTimer.start();
	}
	
	
	/**
	 * Called every time through the loop in Autonomous mode
	 */
	public void Step()
	{
		SmartDashboard.putString("Autonomous State", currentState.toString());
		
		switch(selectedAutonomousDrive)
		{
			case NONE:
				break;
			case DRIVE_MIDDLE:
				stepDriveStraight();
				break;
			case DRIVE_RIGHT:
				stepDriveAndTurn();
				break;
			case DRIVE_LEFT:
				stepDriveAndTurn();
				break;
		}	
	}

	/**
	 * Step method for center position
	 */
	private void stepDriveStraight() {
		
		switch (currentState) {
			case START:
				nextState = autonomousStep.DRIVE;
				timesInState = 0;
				break;
			case DRIVE:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.setDriveSource(m_robot.lidar_pwm);
					m_robot.automatic_drive.drivePIDLoop.setAbsoluteTolerance(0.5);
					m_robot.automatic_drive.driveToDistance(3, SensorDirection.REAR);
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isDriveOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.LINE_UP;
					
					// If the pixy sees the target line up on it, otherwise try and find it
					// f(m_robot.pixy_front.getCurrentframes().size() > 0)
					// {
					// 	nextState = autonomousStep.LINE_UP;
					// } else {
					// 	nextState = autonomousStep.LINE_UP;
					// }
				}
				
				break;
				/*case FIND_TARGET:
					if(timesInState == 0)
					{
						m_robot.automatic_drive.stopDrive();
					}
					
					timesInState++;
					
					// Update the success test for if the pixy sees two targets
					successBoolean.update(m_robot.pixy_front.getCurrentframes().size() >= 2);
					
					if(successBoolean.get())
					{
						m_robot.automatic_drive.stopDrive();
						nextState = autonomousStep.LINE_UP;
					}
					
					break;*/
			case LINE_UP:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.strafePIDLoop.setAbsoluteTolerance(0.15);
					m_robot.automatic_drive.turnPIDLoop.setAbsoluteTolerance(5);
					m_robot.automatic_drive.lineUpOnTarget();
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isStrafeOnTarget() && m_robot.automatic_drive.isTurnOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					
					double dist = 0;
					
					switch(m_robot.pixy_front.getNumObjectsDetected())
					{
						//case 0:
						//	nextState = autonomousStep.STOP;
						//	break;
						case 1:
							
							dist = PixyCmu5.getCalcDistance(m_robot.pixy_front.getTotalArea(1), AutoDriver.m_front_one_tgt_area_constant);
							if(dist < 2)
							{
								nextState = autonomousStep.DELIVER;
							} else {
								nextState = autonomousStep.APPROACH;
							}
								
							break;
						case 2:
							
							dist = PixyCmu5.getCalcDistance(m_robot.pixy_front.getTotalArea(2), AutoDriver.m_front_two_tgt_area_constant);
							if(dist < 2)
							{
								nextState = autonomousStep.DELIVER;
							} else {
								nextState = autonomousStep.APPROACH;
							}
							
							break;
						default:
							// TODO: Decide when to go into approach
							nextState = autonomousStep.APPROACH;
							break;
					}
				}
				
				break;
				
			case APPROACH:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.setDriveSource(m_robot.pixy_front);
					m_robot.automatic_drive.drivePIDLoop.setAbsoluteTolerance(0.5);
					m_robot.automatic_drive.strafePIDLoop.setAbsoluteTolerance(0.15);
					m_robot.automatic_drive.turnPIDLoop.setAbsoluteTolerance(5);					
					m_robot.automatic_drive.driveToDistance(1.5, SensorDirection.FRONT);
					m_robot.automatic_drive.lineUpOnTarget();
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isStrafeOnTarget() && m_robot.automatic_drive.isTurnOnTarget() && m_robot.automatic_drive.isDriveOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.ALIGN;
				}
				break;
				
			case ALIGN:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.lineUpOnTarget();
					m_robot.automatic_drive.strafePIDLoop.setAbsoluteTolerance(0.1);
					m_robot.automatic_drive.turnPIDLoop.setAbsoluteTolerance(2);
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isStrafeOnTarget() && m_robot.automatic_drive.isTurnOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.DELIVER;
				}
				break;
				
			case DELIVER:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.setDriveSource(m_robot.pixy_front);
					m_robot.automatic_drive.drivePIDLoop.setAbsoluteTolerance(0.5);
					m_robot.automatic_drive.driveToDistance(0.2, SensorDirection.FRONT);
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isDriveOnTarget());
				
				if(successBoolean.get() || timer.get() > 3)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.STOP;		
				}
				
				break;
			
			case STOP:
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					timer.reset();
					timer.start();
				} timesInState++;
				
				if(m_robot.automatic_drive.isRunning())
					m_robot.automatic_drive.stopDrive();
				
				// Lower the gear if its still present after x seconds
				if(timer.get() > 3 && 
						m_robot.gearMechanism != null &&
						m_robot.gearMechanism.getCurrentGearState() == GearState.GEAR_PRESENT)
				{
					nextState = autonomousStep.LOWERGEAR;		
				}
				
				break;
				
			case LOWERGEAR:
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					
					if(m_robot.gearMechanism != null)
						m_robot.gearMechanism.lowerGear();
					
					timer.reset();
					timer.start();
				} timesInState++;
				
				if(timer.get() > 2)
				{
					nextState = autonomousStep.RAISEGEAR;
				}
				
				break;
				
			case RAISEGEAR:
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveStraight - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					
					if(m_robot.gearMechanism != null)
						m_robot.gearMechanism.raiseGear();
					
					timer.reset();
					timer.start();
				} timesInState++;
				
				
				if(timer.get() > 2)
				{
					if(m_robot.gearMechanism != null)
					{
						if(m_robot.gearMechanism.getCurrentGearState() == GearState.GEAR_PRESENT)
						{
							nextState = autonomousStep.LOWERGEAR;
						} else if(m_robot.gearMechanism.getCurrentGearState() == GearState.GEAR_NOT_PRESENT) {
							nextState = autonomousStep.STOP;
						}
					}
				}
				
				break;
				
			default:
				nextState = autonomousStep.STOP;
				break;
			
		}
		
		if(currentState != nextState)
		{
			timesInState = 0;
		}
		
		currentState = nextState;
	} // end drive straight
	
	/**
	 * Step method for left or right starting positions
	 */
	private void stepDriveAndTurn() {
		
		switch (currentState) {
			case START:
				nextState = autonomousStep.DRIVE;
				timesInState = 0;
				break;
			case DRIVE:
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.setDriveSource(m_robot.lidar_pwm);
					m_robot.automatic_drive.drivePIDLoop.setAbsoluteTolerance(0.5);
					m_robot.automatic_drive.driveToDistance(6.5, SensorDirection.REAR);
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isDriveOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{

					//m_robot.automatic_drive.drivePIDLoop.setOutputRange(-1.0,1.0);
					nextState = autonomousStep.TURN_TO_ANGLE;
				}
				
				/*
				if(timesInState == 0)
				{
					timer.start();
					m_robot.driveTrain.mecanumDrive_Cartesian(0.0, 0.3, (-m_robot.m_imu.getYaw())/180, 0);
				} timesInState++;
				
				if(timer.get() >= 3)
				{
					nextState = autonomousStep.TURN_TO_ANGLE;
				}*/
				
				break;
				
			case TURN_TO_ANGLE:
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.rotatePIDLoop.setAbsoluteTolerance(5);
					timer.reset();
					timer.start();
					switch(selectedAutonomousDrive)
					{
						case DRIVE_LEFT:
							m_robot.automatic_drive.turnToAngle(-50, true);
							break;
							
						case DRIVE_RIGHT:
							m_robot.automatic_drive.turnToAngle(50, true);
							break;
							
						default:
							nextState = autonomousStep.LINE_UP;
							break;
					}

				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isRotateOnTarget());
				
				if(successBoolean.get() || timer.get() > 1.5)
				{
					m_robot.automatic_drive.stopDrive();
					
					double dist = 0;
					
					switch(m_robot.pixy_front.getNumObjectsDetected())
					{
						case 0:
							nextState = autonomousStep.STOP;
							
							break;
						case 1:
							
							dist = PixyCmu5.getCalcDistance(m_robot.pixy_front.getTotalArea(1), AutoDriver.m_front_one_tgt_area_constant);
							if(dist < 2)
							{
								nextState = autonomousStep.ALIGN;
							} else {
								nextState = autonomousStep.APPROACH;
							}
								
							break;
						case 2:
							
							dist = PixyCmu5.getCalcDistance(m_robot.pixy_front.getTotalArea(2), AutoDriver.m_front_two_tgt_area_constant);
							if(dist < 2)
							{
								nextState = autonomousStep.ALIGN;
							} else {
								nextState = autonomousStep.APPROACH;
							}
							
							
							break;
						default:
							// TODO: Decide when to go into approach
							nextState = autonomousStep.APPROACH;
							break;
					}
							
					// If the pixy sees the target line up on it, otherwise try and find it
					/*if(m_robot.pixy_front.getCurrentframes().size() > 0)
					{
						nextState = autonomousStep.LINE_UP;
					} else {
						nextState = autonomousStep.LINE_UP;
					}*/
				}
				
				break;
			/*case FIND_TARGET:
				if(timesInState == 0)
				{
					m_robot.automatic_drive.stopDrive();
				} timesInState++;
				
				// Update the success test for if the pixy sees two targets
				successBoolean.update(m_robot.pixy_front.getCurrentframes().size() >= 2);
				nextState = autonomousStep.LINE_UP;
				if(successBoolean.get())
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.LINE_UP;
				}
				
				break;*/
			case LINE_UP:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.strafePIDLoop.setAbsoluteTolerance(0.15);
					m_robot.automatic_drive.turnPIDLoop.setAbsoluteTolerance(4);
					m_robot.automatic_drive.lineUpOnTarget();
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isStrafeOnTarget() && m_robot.automatic_drive.isTurnOnTarget() );
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.APPROACH;
				}
				
				break;
				
			case APPROACH:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.drivePIDLoop.reset(); // TODO: Remove?
					m_robot.automatic_drive.setDriveSource(m_robot.pixy_front);
					m_robot.automatic_drive.drivePIDLoop.setAbsoluteTolerance(0.5);
					m_robot.automatic_drive.strafePIDLoop.setAbsoluteTolerance(0.15);
					m_robot.automatic_drive.turnPIDLoop.setAbsoluteTolerance(5);
					m_robot.automatic_drive.driveToDistance(1.5, SensorDirection.FRONT);
					m_robot.automatic_drive.lineUpOnTarget();
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isStrafeOnTarget() && m_robot.automatic_drive.isTurnOnTarget() && m_robot.automatic_drive.isDriveOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.ALIGN;
				}
				break;
			case ALIGN:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.strafePIDLoop.setAbsoluteTolerance(0.15);
					m_robot.automatic_drive.turnPIDLoop.setAbsoluteTolerance(2);
					m_robot.automatic_drive.lineUpOnTarget();
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isStrafeOnTarget() && m_robot.automatic_drive.isTurnOnTarget());
				
				if(successBoolean.get() || timer.get() > 2)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.DELIVER;
				}
				break;
			case DELIVER:
				
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
					m_robot.automatic_drive.setDriveSource(m_robot.pixy_front);
					m_robot.automatic_drive.driveToDistance(0.2, SensorDirection.FRONT);
					timer.reset();
					timer.start();
				} timesInState++;
				
				successBoolean.update(m_robot.automatic_drive.isDriveOnTarget());
				
				if(successBoolean.get() || timer.get() > 5)
				{
					m_robot.automatic_drive.stopDrive();
					nextState = autonomousStep.STOP;
				}
				
				break;
	
			case STOP:
				if(timesInState == 0)
				{
					System.out.println("Autonomous::stepDriveAndTurn - " + currentState.toString());
					m_robot.automatic_drive.stopDrive();
				} timesInState++;
				
				if(m_robot.automatic_drive.isRunning())
				{
					m_robot.automatic_drive.stopDrive();
				}
				
				break;
			default:
				nextState = autonomousStep.STOP;
				break;
			
		}
		
		if(currentState != nextState)
		{
			timesInState = 0;
		}
		
		currentState = nextState;
	}
	
	/**
	 * Stop drives and finish autonomous
	 */
	public void Stop()
	{
		nextState = autonomousStep.START;
		currentState = autonomousStep.START;
		
		if(m_robot.automatic_drive != null)
		{
			m_robot.automatic_drive.stopDrive();
		}
	}
	
	/**
	 * @return the selectedAutonomous
	 */
	public synchronized AutonomousDriveStep getSelectedAutonomousDrive() {
		return selectedAutonomousDrive;
	}

	/**
	 * @param selectedAutonomous the selectedAutonomous to set
	 */
	public synchronized void setSelectedAutonomousDrive(AutonomousDriveStep selectedAutonomous) {
		this.selectedAutonomousDrive = selectedAutonomous;
	}
	
	/**
	 * @return the selectedAutonomous
	 */
	public synchronized AutonomousShootStep getSelectedAutonomousShoot() {
		return selectedAutonomousShoot;
	}

	/**
	 * @param selectedAutonomous the selectedAutonomous to set
	 */
	public synchronized void setSelectedAutonomousShoot(AutonomousShootStep selectedAutonomous) {
		this.selectedAutonomousShoot= selectedAutonomous;
	}
	
	// TODO: Test to see if method even works for demo purposes
	
	/*
    public void letsDance(){
    	Timer danceTimer = new Timer();
    	boolean doWeWantToDance = true;
    	SmartDashboard.putString("Let's Dance", "LET'S DANCE!!!");
    	danceTimer.start();
    		while(doWeWantToDance){
		   		if(danceTimer.get() <= 500){//(~^.^)~
		   				driveTrain.mecanumDrive_Cartesian(0,0.5,0,0);
		   				motor_LS.set(.2);
	    		  		motor_RS.set(0.0);
	    		  		actuator.setPosition(.39);
		   			}
		  			if(danceTimer.get()  <= 1000){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0,-0.5,0.35,0);
		  				motor_LS.set(0);
	    		  		motor_RS.set(.2);
	    		  		actuator.setPosition(.6);
		  			}
		  			else if(danceTimer.get()  <= 1500){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0,0.2,-0.4,0);
		  				motor_LS.set(0);
	    		  		motor_RS.set(0);
	    		  		actuator.setPosition(0.725);
		  			}
		  			else if(danceTimer.get()  <= 2000){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0.6,0,0,0);
		  				motor_LS.set(.2);
	    		  		motor_RS.set(.2);
	    		  		actuator.setPosition(0.6);
		  			}
		  			else if(danceTimer.get()  <= 2500){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(-0.35,0,.25,0);
		  				motor_LS.set(.4);
	    		  		motor_RS.set(.4);
	    		  		actuator.setPosition(0.39);
		  			}
		  			else if(danceTimer.get()  <= 3000){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0.3,0.3,0,0);
		  				motor_LS.set(.6);
	    		  		motor_RS.set(.6);
	    		  		actuator.setPosition(0.6);
		  			}
		  			
		  			else if(danceTimer.get()  <= 3500){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(-0.3,-0.3,0,0);
		  				motor_LS.set(.8);
	    		  		motor_RS.set(.8);
	    		  		actuator.setPosition(0.35);
		  			}
		  			else if(danceTimer.get()  <= 4000){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0,0,0.2,0);
		  				motor_LS.set(0);
	    		  		motor_RS.set(0);
	    		  		actuator.setPosition(0.6);
		  			}
		  			else if(danceTimer.get()  <= 4500){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0,0,-0.2,0);
		  				motor_LS.set(0);
	    		  		motor_RS.set(.1);
	    		  		actuator.setPosition(0.725);
		  			}
		  			else if(danceTimer.get()  <= 5000){//(~^.^)~
		  				driveTrain.mecanumDrive_Cartesian(0.5,0.5,0.5,0);
		  				motor_LS.set(.1);
	    		  		motor_RS.set(0);
	    		  		actuator.setPosition(0.6);
		  			}
		  			else if(db_danceStart.get()){
		  				doWeWantToDance = false;
		  				SmartDashboard.putString("Cancel Dance","You're a party pooper.");
		  			}
		  			else{
						doWeWantToDance = false;// :(
						driveTrain.mecanumDrive_Cartesian(0,0,0,0);
						danceTimer.stop();
						danceTimer.reset();
						motor_LS.set(0);
	    		  		motor_RS.set(0);
	    		  		actuator.setPosition(0.39);
		  			}
				} // close doWeWantToDance check  
    	} // close letsDance()
   	*/
	
}
