package org.usfirst.frc.team702.robot;

import org.usfirst.frc.team702.pidtools.AnglePIDController;
import org.usfirst.frc.team702.pidtools.PIDOutputTarget;
import org.usfirst.frc.team702.sensors.PixyCmu5;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

// Extend the RobotDrive to override @c mecanumDrive_Cartesian to add rate limits to the inputs.
/**
 * @author sbaron
 *
 */
public class RAWDrive extends RobotDrive {

	double m_gamma = 2.0;
	
	// Setpoints to trigger autonomous movement
	private boolean m_enableAnglePID = false;
	private boolean m_enableOffsetPID = false;

	public double angleSetpoint;
	public double offsetSetpoint;
	// Rotation control.
	AHRS navX;

	boolean needsTarget;
	float m_targetRotation = 0;

	// PID Controllers
	AnglePIDController anglePIDLoop;
	PIDOutputTarget    turnCommand;
	
	PIDController offsetPIDLoop;
	PIDOutputTarget strafeCommand;
	
	// PID Constants
	double kP = .028, kI = 0.00, kD = 0.05, kF = 0.0;
	double kToleranceDegrees = 2.0;
	double kToleranceOffsetArea = 200;

	private volatile double turnRate;
	private boolean flg_debug = true;

	private PixyCmu5 m_pixy;

	public RAWDrive(SpeedController frontLeftMotor,
			SpeedController rearLeftMotor, SpeedController frontRightMotor,
			SpeedController rearRightMotor) {
		super(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);
		
		strafeCommand = new PIDOutputTarget();
		turnCommand   = new PIDOutputTarget();
	}

	/**
	 * Initializes the turning PID Loop
	 */
	private void initAnglePIDLoop(PIDSource source, PIDOutputTarget target)
	{
		anglePIDLoop = new AnglePIDController(kP, kI, kD, source, target);
		anglePIDLoop.setInputRange(-180.0, 180.0);
		anglePIDLoop.setOutputRange(-1.0, 1.0);
		anglePIDLoop.setAbsoluteTolerance(kToleranceDegrees);
		anglePIDLoop.setToleranceBuffer(1);
		anglePIDLoop.setContinuous(true);

		/* Add the PID Controller to the Test-mode dashboard, allowing manual  */
        /* tuning of the Turn Controller's P, I and D coefficients.            */
        /* Typically, only the P value needs to be modified.                   */
        //LiveWindow.addActuator("DriveSystem", "RotateController", anglePIDLoop);
	}
	
	/**
	 * Initializes the offset PID Loop
	 * @param source
	 * @param target
	 */
	private void initOffsetPIDLoop(PIDSource source, PIDOutputTarget target)
	{
		offsetPIDLoop = new AnglePIDController(kP, kI, kD, source, target);
		offsetPIDLoop.setInputRange(-2000,2000);
		offsetPIDLoop.setOutputRange(-1.0, 1.0);
		offsetPIDLoop.setAbsoluteTolerance(kToleranceOffsetArea);
		offsetPIDLoop.setToleranceBuffer(5);
		offsetPIDLoop.setContinuous(false);

		/* Add the PID Controller to the Test-mode dashboard, allowing manual  */
        /* tuning of the Turn Controller's P, I and D coefficients.            */
        /* Typically, only the P value needs to be modified.                   */
        LiveWindow.addActuator("DriveSystem", "StrafeController", offsetPIDLoop);
	}
		
	/**
	 * Indicates if the PID loop is enabled and running.
	 * 
	 * @return boolean
	 */
	public boolean isAutoEnable() {
		if(this.navX == null)
		{
			System.out.println("RESET NAVX!!!!!!!!!!!!!!!!!!!!!!");
			return false;
		}
			
		if(anglePIDLoop.isEnabled() && m_enableAnglePID)
		{
			return true;
		} else {
			// If there is a conflict in the settings, fix them
			this.setAutoEnable(false);
			return false;
		}
	}

	/**
	 * Indicates if the PID loop error is within tolerance
	 * i.e., The robot has turned to the correct angle
	 * 
	 * @return boolean
	 */
	public boolean isAngleComplete()
	{
		if(flg_debug) {
			System.out.println( anglePIDLoop.onTarget() == true ? "Complete" : "Turning" + " - Error: " + anglePIDLoop.getError());
		}
		
		return anglePIDLoop.onTarget();
	}
	
	/*
	 * Indicates if the PID loop error is within tolerance
	 * i.e., The robot has strafed to the correct position
	 * 
	 * @return boolean
	 */
	public boolean isStrafeComplete()
	{
		if(flg_debug) {
			System.out.println( offsetPIDLoop.onTarget() == true ? "Complete" : "Strafing" + " - Error: " + offsetPIDLoop.getError());
		}
		
		return offsetPIDLoop.onTarget();
	}

	/**
	 * Set an angle setpoint for the auto-rotate target. The angle 
	 * is error checked and normalized to insure it is within bounds.
	 * 
	 * @param angleSetpoint
	 */
	public void setAngleSetpoint(double angleSetpoint) {
		this.angleSetpoint = RAWDrive.normalizeAngle((float)angleSetpoint);
	}
	
	/**
	 * Sets the offset setpoint for the strafe
	 * 
	 * @param offset
	 */
	public void setOffsetSetpoint(double offset)
	{
		this.offsetSetpoint = offset;
	}

	/**
	 * Enables or disables the automatic rotation function and sets the PID target
	 * to the angleSetpoint property if enabling
	 * 
	 * @param autoEnable - boolean
	 */
	public void setAutoEnable(boolean autoEnable)
	{
		
		if(anglePIDLoop == null || navX == null)
		{
			System.out.println("INITIALIZE NAVX!!!");
			return;
		}
		// If autoEnable is true, enable the PID loop and 
		// set the setpoint to what is assigned in the class
		if(autoEnable && anglePIDLoop != null && navX != null)
		{ // Enable the turn controller
			//this.navX.resetDisplacement();
			
			this.anglePIDLoop.reset();
			this.anglePIDLoop.setSetpoint(this.angleSetpoint);
			this.anglePIDLoop.enable();
			
			this.m_enableAnglePID = autoEnable;
			System.out.println("SetAutoEnable::Enabling Turn Controller");
			
		} else { // Disable the turn controller
			System.out.println("SetAutoEnable::Disabling Turn Controller");
			this.anglePIDLoop.reset();
			this.anglePIDLoop.disable();
			this.m_enableAnglePID = autoEnable;
		}
		if (navX == null) {
			System.out.println("~Initialize navx~");
		}
	}

	/**
	 * Assigns the IMU object as the PID source. In this case we expect a navX
	 * Error checking is done to insure that the IMU object has been initalized
	 * 
	 * @param imu - AHRS
	 * @return - true for success, false for failure
	 */
	public boolean setImu(AHRS imu) {
		if(navX == null && imu != null)
		{
			navX = imu;
	        System.out.println("Navx Added Successfully");
	        initAnglePIDLoop(navX,turnCommand);
	        
			return true;
		} else {
			System.out.println("Error Adding Navx!");
			return false;
		}
	}
	
	/**
	 * Assigns the IMU object as the PID source. In this case we expect a navX
	 * Error checking is done to insure that the IMU object has been initialized
	 * 
	 * @param imu - AHRS
	 * @return - true for success, false for failure
	 */
	public boolean setPixy(PixyCmu5 pixy) {
		if(m_pixy == null && pixy != null)
		{
			m_pixy = pixy;
	        System.out.println("Pixy Added Successfully");
	        initOffsetPIDLoop(m_pixy, strafeCommand);
	        
			return true;
		} else {
			System.out.println("Error Adding Navx!");
			return false;
		}
	}

	/**
	 * Drive straight forward or backward and use the navx to compensate for drift
	 * @param speed
	 */
	public void driveStraight(double speed)
	{
		if(navX != null)
		{
			this.drive(speed, (navX.getYaw()/(m_gamma*180)));
		}
	}

	/**
	 * Return the turn rate for the drive command
	 * 
	 */
	public synchronized double getTurnRate()
	{
		if(this.anglePIDLoop.isEnabled())
		{
			if(this.m_frontLeftMotor.getInverted()) {
				return -this.turnCommand.getOutput();
			} else {
				return this.turnCommand.getOutput();
			}
		} else {
			return 0.0;
		}
	}
	
	/**
	 * Sets the PID setpoint and enables the PID loop to turn the robot to 
	 * the specified angle
	 * 
	 * @param targetAngle - the target angle, automatically normalized
	 * @param relative - if true, set the angle relative to the robots current heading
	 */
	public void turnToAngle(float targetAngle, boolean relative)
	{
		
		if(navX == null)
		{
			System.out.println("NAVX NEEDS TO BE INITIALIZED!!");
			return;
		}
		
		// If the relative flag is set, add the targetAngle to the current angle
		if(relative)
		{
			targetAngle = normalizeAngle(navX.getYaw()+normalizeAngle(targetAngle));
		}
		
		System.out.println("Turning to angle: " + Float.toString(targetAngle));

		// Assign the target angle (now absolute) to the angleSetpoint
		this.setAngleSetpoint(targetAngle);
		
		System.out.println("Angle setpoint: " + Double.toString(this.angleSetpoint));
		
		if(!anglePIDLoop.isEnabled())
		{
			this.setAutoEnable(true);
		}
	}
	
	public void setkP(double kP)
	{
		this.kP = kP;
		if(this.anglePIDLoop != null)
		{
			this.anglePIDLoop.setPID(this.kP, this.kI, this.kD);
		}
	}
	
	public double getkP()
	{
		if(this.anglePIDLoop != null)
		{
		return this.anglePIDLoop.getP();
		} else {
			return this.kP;
		}
	}
	
	public void setkI(double kI)
	{
		this.kI = kI;
		if(this.anglePIDLoop != null)
		{
		this.anglePIDLoop.setPID(this.kP, this.kI, this.kD);
		}
	}
	
	public double getkI()
	{
		if(this.anglePIDLoop != null)
		{
		return this.anglePIDLoop.getI();
		} else {
			return this.kI;
		}
	}
	
	public void setkD(double kD)
	{
		this.kD = kD;
		if(this.anglePIDLoop != null)
		{
		this.anglePIDLoop.setPID(this.kP, this.kI, this.kD);
		}
	}
	
	public double getkD()
	{
		if(this.anglePIDLoop != null)
		{
		return this.anglePIDLoop.getD();
		} else {
			return this.kD;
		}
	}
	
	public void initSmartDashboard()
	{
		// RAWDrive PID settings
		//SmartDashboard.putBoolean("AutoRotateEnabled", false);
		//SmartDashboard.putBoolean("On Target", true);
		//SmartDashboard.putNumber("Error", 0);
		//SmartDashboard.putNumber("Setpoint", 0);
		//SmartDashboard.putNumber("Avg. Error", 0);
		SmartDashboard.putNumber("Drive Output", 0);
		//SmartDashboard.putNumber("Angle Reading", 0)	;
	}
	
	public void updateSmartDashboardData()
	{
		//SmartDashboard.putBoolean("AutoRotateEnabled", anglePIDLoop.isEnabled());
		//SmartDashboard.putBoolean("On Target", anglePIDLoop.onTarget());
		//SmartDashboard.putNumber("Error", anglePIDLoop.getError());
		//SmartDashboard.putNumber("Setpoint", anglePIDLoop.getSetpoint());
		//SmartDashboard.putNumber("Avg. Error", anglePIDLoop.getAvgError());
		SmartDashboard.putNumber("Drive Output", turnRate);
		//SmartDashboard.putNumber("Angle Reading", navX.getYaw());
	}
	
	public void getSmartDashboardData()
	{
		double tmpP = SmartDashboard.getNumber("P", -1);
		if(tmpP != -1)
		{
			setkP(tmpP);
		}
		
		double tmpI = SmartDashboard.getNumber("I", -1);
		if(tmpI != -1)
		{
			setkI(tmpI);
		}
		
		double tmpD = SmartDashboard.getNumber("D", -1);
		if(tmpD != -1)
		{
			setkD(tmpD);
		}

		/*
		 * TODO: implement this.setkF()
		double tmpF = SmartDashboard.getNumber("raw_F", -1);
		if(tmpF != -1)
		{
			setkF(tmpF);
		}*/
		
	}
	
	// Returns an angle between -180 and 180 degrees
	public static float normalizeAngle(float angle)
	{
		// reduce the angle  
		angle =  angle % 360; 

		// force it to be the positive remainder, so that 0 <= angle < 360  
		angle = (angle + 360) % 360;  

		// force into the minimum absolute value residue class, so that -180 < angle <= 180  
		if (angle > 180)  
			angle -= 360;  

		return angle;
	}
}

