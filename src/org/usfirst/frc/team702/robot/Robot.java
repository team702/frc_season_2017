package org.usfirst.frc.team702.robot; // package of classes provided by FIRST for the FRC competition

	/**
	 * @authors Ezra "Zelda", Keoni "Twilight Sparkle", Ben, Scott "Azules, and Sabri "Rocket"
	 * @coauthors Simon, Kelli "Jynx", and Maki "Weaboo"
	 */

import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.RobotDrive;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.usfirst.frc.team702.robot.Autonomous.AutonomousDriveStep;
import org.usfirst.frc.team702.robot.Autonomous.AutonomousShootStep;
import org.usfirst.frc.team702.sensors.LidarLite;
import org.usfirst.frc.team702.sensors.LidarLitePWM;
import org.usfirst.frc.team702.sensors.PixyCmu5;
import org.usfirst.frc.team702.sensors.PixyCmu5.PixyFrame;
import org.usfirst.frc.team702.sensors.Ultrasonic;
import org.usfirst.frc.team702.tools.DebouncedBoolean;
import org.usfirst.frc.team702.tools.I2C_Scanner;
import org.usfirst.frc.team702.tools.OneShotBoolean;

import com.ctre.CANTalon;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.filters.LinearDigitalFilter;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Robot extends SampleRobot { // begin class Robot
	
	// Configuration flags
	private final boolean debug = false;
	private final boolean practice = false;
	private final boolean competition = true;
	
	// CANTalons for drive system
	public CANTalon motor_FL; // motor_FL is an arbitrary name. It can be anything. The name is a unique object ID so the code can find and use the exact object you want.
	public CANTalon motor_FR; // CANTalon might startle some of you. That is okay, the CANTalon is a more advanced type of Talon. I will explain more further down.
	public CANTalon motor_RL;
	public CANTalon motor_RR;
	
	// CANTalons for climb system
	public CANTalon motor_CL;
	
	// CANTalon(s) for shooting system
	public CANTalon motor_LS;
	public CANTalon motor_ConveyerLS;
	public CANTalon motor_RS;
	public CANTalon motor_ConveyerRS;
	
	// actuator for gear delivery system
	public Servo actuator;
	public CANTalon gearTalon;
	
	// Gear sensor
	public AnalogInput gearSensor;
	public AnalogInput ultrasonic;
	public Ultrasonic ultrasonic_sensor;
	
	// placeholder CANTalon
	private CANTalon placeholder;
	
	// controllers
	private Joystick controller;
	private Joystick coController;
	
	// RobotDrives
	public RobotDrive driveTrain; // The conventional naming scheme for all objects/variables is camel case (exampleCamelCase). First word is lower, then upper.
	public RobotDrive climb;
	
	public GearDelivery gearMechanism;
	
	// navx
	public AHRS m_imu;
	// Pixy
	public PixyCmu5 pixy_front;
	public PixyCmu5 pixy_rear;
	
	// LidarLite_V1
	public LidarLite lidar_front;
	public LidarLite lidar_rear;
	public LidarLitePWM lidar_pwm;
	private ReentrantLock mxp_i2c_mutex;
	private ReentrantLock main_i2c_mutex;
	
	// Moving averate filters to clean up the pixy angle reading
	private LinearDigitalFilter pixyFrontAngleFilter;
	private LinearDigitalFilter pixyRearAngleFilter;
	
	 // Class variable for autonomous
    private Autonomous autonomousMode;
	
	// I2C Finder
	I2C_Scanner I2C_Scanner;
	
	// double variables for controllers
	double lsyA;
	double rsxA;
	double lsxA;
	double zTrigL;
	double zTrigR;
	double zTrigTot;
	
	double co_lsyA;
	double co_rsxA;
	double co_rsyA;
	double co_lsxA;
	double co_zTrigL;
	double co_zTrigR;
	double co_zTrigTot;
	
	
	// booleans for buttons | OneShotBooleans for efficient buttons (object)
	boolean buttonA;
	boolean buttonB;
	boolean buttonX;
	boolean buttonY;
	boolean buttonSt;
	boolean [] invert = new boolean [4];
	
	public OneShotBoolean db_buttonRB;
	public OneShotBoolean db_buttonSt;
	public OneShotBoolean db_buttonB;
	public OneShotBoolean db_buttonLB;
	public OneShotBoolean db_buttonA;
	public OneShotBoolean db_buttonX;
	public OneShotBoolean db_buttonY;
	
	//public OneShotBoolean db_Co_buttonRB;
	public OneShotBoolean db_Co_buttonSt;
	public OneShotBoolean db_Co_buttonB;
	public OneShotBoolean db_Co_buttonLB;
	public DebouncedBoolean db_CobuttonRB2;
	public DebouncedBoolean db_CobuttonLB2;
	
	public OneShotBoolean db_Co_buttonA;
	public OneShotBoolean db_Co_buttonX;
	public OneShotBoolean db_Co_buttonY;
	
	public OneShotBoolean db_danceStart;
	
	public double drive_forward_for = 0;
	public double turn_to_angle = 0;
	public double rotate_at_rate = 0;
	
	// Option box for the autonomous mode selection
	SendableChooser<AutonomousDriveStep> autoModeDriveChooser;
	SendableChooser<AutonomousShootStep> autoModeShootChooser;
	AutonomousDriveStep selectedDriveAutonomous;
	AutonomousDriveStep selectedShootAutonomous;
	
	CameraServer frontCam;
	
	//I2C_Scanner i2cscan;
	
	double shooterSpeedL = 0.73847;
	double shooterSpeedR = 0.84575;
	private double pixyReadRate = 0.04;
	private double lidarReadRate = 0.01;
	
	public AutoDriver automatic_drive;
	private Timer modeTimer;
	
	// Cameras
	UsbCamera camera1;
//	UsbCamera camera2;
	
    public Robot() { // begin constructor
        
    	if (competition) {
    		// drive motors
        	this.motor_FL = new CANTalon(20); // CANTalons take an argument of a specific ID number you can identify within the cRIO itself (more on that later)
            this.motor_FR = new CANTalon(22);
            this.motor_RL = new CANTalon(21);
            this.motor_RR = new CANTalon(23);
            
            // motor for climb system (CANTalon)
            this.motor_CL = new CANTalon(24);
            this.motor_CL.setInverted(true);
            
            // motor for shooting system (CANTalon)
            this.motor_LS = new CANTalon(25); // convert to final robot ID
            this.motor_ConveyerLS = new CANTalon(26); // convert to final robot ID
            this.motor_RS = new CANTalon(27);
            this.motor_ConveyerRS = new CANTalon(29);
    	}
    	
    	if (practice) {
    		// drive motors
        	this.motor_FL = new CANTalon(20); // CANTalons take an argument of a specific ID number you can identify within the cRIO itself (more on that later)
            this.motor_FR = new CANTalon(22);
            this.motor_RL = new CANTalon(21);
            this.motor_RR = new CANTalon(24);
            
            // motor for climb system (CANTalon)
            this.motor_CL = new CANTalon(23);
            
            this.motor_LS = new CANTalon(25); // convert to final robot ID
            this.motor_LS.setInverted(true);
            this.motor_ConveyerLS = new CANTalon(26); // convert to final robot ID
            
            this.motor_RS = new CANTalon(27);
            this.motor_RS.setInverted(true);
            this.motor_ConveyerRS = new CANTalon(28);
    	}
    	
    	// TODO: Switch FR to FL when you arrive at St. Luis
    	
        // invert left side of drive base so all wheels go in the same direction
        this.motor_FL.setInverted(true);
        this.motor_RL.setInverted(true);
        
        // controllers
        this.controller = new Joystick(0); // The Joystick object takes the argument of the order the Joystick is plugged in. This means that the Joystick in
        								   // question is the first one plugged in.
        this.coController = new Joystick(1); // coDriver
        //this.danceController = new Joystick(2);
        
        // TODO: Switch FR and FL when you arrive at St. Luis
        
        // RobotDrives
        this.driveTrain = new RAWDrive(motor_FL, motor_RL, motor_FR, motor_RR); // The RobotDrive object takes in SpeedController objects to create a system of
        																  // SpeedControllers that run all at the same time (creating a drive base).
        																  // The RobotDrive takes motors in a specific order. Hover over it to see more.
        // shooter inversion
        this.motor_RS.setInverted(true);
        
        // conveyer inversion
        this.motor_ConveyerLS.setInverted(true);
        
        // linear actuator for gear delivery system
        this.actuator = new Servo(0);
        this.gearSensor = new AnalogInput(0);
        this.gearTalon = new CANTalon(28);
        this.gearTalon.setInverted(true);
        
        this.ultrasonic = new AnalogInput(1);
        this.ultrasonic_sensor = new Ultrasonic(ultrasonic,0.2977);
        
        this.gearMechanism = new GearDelivery(gearTalon, gearSensor);
        
        // placeholder CANTalon for single motor RobotDrive
        this.placeholder = new CANTalon(35);
        
        // climbing system RobotDrive
        this.climb = new RobotDrive(motor_CL, placeholder);
        
        // one-shot driver boolean(s)
        this.db_buttonLB = new OneShotBoolean(10);
        this.db_buttonRB = new OneShotBoolean(10);
        this.db_buttonSt = new OneShotBoolean(10);
        this.db_buttonB = new OneShotBoolean(10);
        this.db_buttonA = new OneShotBoolean(10);
        this.db_buttonX = new OneShotBoolean(10);
        this.db_buttonY = new OneShotBoolean(10);
        
        //one-shot co-driver boolean(s)
        this.db_Co_buttonLB = new OneShotBoolean(10);
        this.db_CobuttonLB2 = new DebouncedBoolean(10);
        this.db_CobuttonRB2 = new DebouncedBoolean(10);
        //this.db_Co_buttonRB = new OneShotBoolean(10);
        this.db_Co_buttonSt = new OneShotBoolean(10);
        this.db_Co_buttonB = new OneShotBoolean(10);
        this.db_Co_buttonA = new OneShotBoolean(10);
        this.db_Co_buttonX = new OneShotBoolean(10);
        this.db_Co_buttonY = new OneShotBoolean(10);
        
        //one-shot dancing boolean
        this.db_danceStart = new OneShotBoolean(10);
        
		for (int index = 0; index < invert.length; index++) {
			invert[index] = false;
			/**if (index == 0 || index == 2){
				invert[index] = false;
			}else{ 
				invert[index] = true;
			}
			*/
		} // end for
		
		this.modeTimer = new Timer();
		
		//this.i2cscan = new I2C_Scanner(I2C.Port.kOnboard, null);
		
    } // end constructor
    
    public void robotInit() { // begin robot
    	
    	// TODO: Switch kMXP to kUSB1
    	
    	// Initialize NavX Objects
    	try {
	    	//microNAVX = new AHRS(SerialPort.Port.kUSB1);
	    	m_imu = new AHRS(SerialPort.Port.kUSB1);
	    	
	    } catch (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating navX:  " + ex.getMessage(), true);
	    }
    	
    	// Initialize the mutexes for shared I2C Bus
    	mxp_i2c_mutex = new ReentrantLock();
    	main_i2c_mutex = new ReentrantLock();
    	
    	// Initialize Pixy Objects
    	try {
    	    pixy_front = new PixyCmu5(PixyCmu5.PIXY_I2C_DEFAULT_ADDR, I2C.Port.kOnboard, pixyReadRate, null);
    	    //pixy_front = new PixyCmu5(new I2C(I2C.Port.kOnboard, PixyCmu5.PIXY_I2C_DEFAULT_ADDR), pixyReadRate, null);
    	    //pixy_rear = new PixyCmu5(new I2C(I2C.Port.kMXP, PixyCmu5.PIXY_I2C_DEFAULT_ADDR), pixyReadRate, mxp_i2c_mutex); 
    	    
    	    // Initialize the linear filter
    	    pixyFrontAngleFilter = LinearDigitalFilter.movingAverage(pixy_front, 10);
    	    //pixyRearAngleFilter = LinearDigitalFilter.movingAverage(pixy_front, 10);
    	    
    	} catch (RuntimeException ex) { // end Pixy try | begin Pixy catch
    		DriverStation.reportError("Unable to Initialize Pixy: " + ex.getMessage(), true);
    	}
    	   	
    	// Initialize LidarLite_V1 Objects    	
    	try {    		
    		//lidar_front = new LidarLite(new I2C(I2C.Port.kOnboard, LidarLite.DEFAULT_I2C_ADDRESS), lidarReadRate, main_i2c_mutex, null, HWVERSION.V1);
    		//lidar_front.configReturn(true, true);
    		//lidar_rear  = new LidarLite(new I2C(I2C.Port.kMXP, LidarLite.DEFAULT_I2C_ADDRESS), lidarReadRate, null, null, null);
    		//lidar_rear.configReturn(true, true);
    	} catch (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating LidarLite:  " + ex.getMessage(), true);
	    }
    	
    	try
    	{
    		lidar_pwm = new LidarLitePWM(new DigitalInput(1));
    		lidar_pwm.setAutomaticMode(true);
    	} catch  (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating LidarLitePWM:  " + ex.getMessage(), true);
    		
    	}
    	    	
    	
    	/**
    	 * Autonomous Mode Setup
    	 */
    	// autonomous mode selection through Java Smart Dashboard
    	autonomousMode = new Autonomous(this);
    	
    	autoModeDriveChooser = new SendableChooser<AutonomousDriveStep>();
	    autoModeDriveChooser.addDefault("No Autonomous", AutonomousDriveStep.NONE);
	    autoModeDriveChooser.addObject("Drive Middle", AutonomousDriveStep.DRIVE_MIDDLE);
	    autoModeDriveChooser.addObject("Drive Left", AutonomousDriveStep.DRIVE_LEFT);
	    autoModeDriveChooser.addObject("Drive Right", AutonomousDriveStep.DRIVE_RIGHT);

	    autoModeShootChooser = new SendableChooser<AutonomousShootStep>();
	    autoModeShootChooser.addDefault("No Shoot", AutonomousShootStep.NONE);
	    autoModeShootChooser.addObject("Shoot Left", AutonomousShootStep.SHOOT_LEFT);
	    autoModeShootChooser.addObject("Shoot Right", AutonomousShootStep.SHOOT_RIGHT);
	    
	    SmartDashboard.putData("Autonomous Drive Mode",autoModeDriveChooser);
	    SmartDashboard.putData("Autonomous Shoot Mode",autoModeShootChooser);
	    
	    /**
	     * Sensor and data variables
	     */
	    // Navx Smart Dashboard
	    SmartDashboard.putNumber("Yaw Deviation", 1.0);
	    SmartDashboard.putNumber("AngleDeviation", 0.0);
	    
	    // Shooter Speed Modifier
	    SmartDashboard.putNumber("ShooterSpeedL", 0.73847);
	    SmartDashboard.putNumber("ShooterSpeedR", 0.84575);
	    
	    this.automatic_drive = new AutoDriver(driveTrain, pixy_front, pixy_front, pixy_front);
		
		if(m_imu != null && automatic_drive != null)
		{
			this.automatic_drive.setImu(m_imu);
		}
	    
	    // I2C Scanner
	    SmartDashboard.putNumber("I2C Search",0);
	    
    } // end robotInit
    
    public void disabled() {
    	while(isDisabled()) {
    		
    		updateFromDashboard(); // Get data from dashboard
    		
    		try {
    			Thread.sleep(5);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    		
    		if(gearMechanism != null)
    		{
    			gearMechanism.setAutonomous(true);
    		}
    		
    		dataToDashboard(); // Update Dashboard    		
    	
    		if(automatic_drive != null)
    		{
    			automatic_drive.stopDrive();
    		}
    	
    	}
    }

    /**
     * Helper function to get data from the dasboard and update local variables
     */
    private void updateFromDashboard()
    {
    	// Get some data from the smart dashboard
    	if(autonomousMode != null)
    	{
    		// Set the auto mode from the dasboard
    		autonomousMode.setSelectedAutonomousShoot((AutonomousShootStep) autoModeShootChooser.getSelected());
    		autonomousMode.setSelectedAutonomousDrive((AutonomousDriveStep) autoModeDriveChooser.getSelected());
    	}
    	
		this.shooterSpeedL 	= SmartDashboard.getNumber("ShooterSpeedL", 0.9);
		this.shooterSpeedR 	= SmartDashboard.getNumber("ShooterSpeedR", 0.9);    		
		this.drive_forward_for = SmartDashboard.getNumber("Drive To Distance",0.0);
		this.turn_to_angle = SmartDashboard.getNumber("Turn To Angle",0.0);
		this.rotate_at_rate = SmartDashboard.getNumber("Rotate At Rate",0.0);
		
		if(pixyReadRate != SmartDashboard.getNumber("Pixy Read Rate", pixyReadRate))
		{
			pixyReadRate = SmartDashboard.getNumber("Pixy Read Rate", pixyReadRate);
			if(pixy_front != null)
			{
				pixy_front.start(pixyReadRate);
			}
			
			if(pixy_rear != null)
			{
				pixy_rear.start(pixyReadRate);
			}
		}
		
		if(lidarReadRate != SmartDashboard.getNumber("Lidar Read Rate", lidarReadRate))
		{
			lidarReadRate = SmartDashboard.getNumber("Lidar Read Rate", lidarReadRate);
			if(lidar_front != null)
			{
				lidar_front.start(lidarReadRate);
			}
			
			if(lidar_rear != null)
			{
				lidar_rear.start(lidarReadRate);
			}
		}
		
		if(automatic_drive != null)
		{
			automatic_drive.getFromDashboard(automatic_drive.turnPIDLoop,"Turn");
			automatic_drive.getFromDashboard(automatic_drive.strafePIDLoop,"Strafe");
			automatic_drive.getFromDashboard(automatic_drive.drivePIDLoop,"Drive");
			automatic_drive.getFromDashboard(automatic_drive.rotatePIDLoop,"Rotate");
		}
		
		if(gearMechanism != null)
		{
			gearMechanism.setServoValueGearUp(SmartDashboard.getNumber("Gear Servo Up Value", gearMechanism.getServoValueGearUp()));
			gearMechanism.setServoValueGearDown(SmartDashboard.getNumber("Gear Servo Down Value", gearMechanism.getServoValueGearDown()));
			gearMechanism.setTimeToWaitAfterDetection(SmartDashboard.getNumber("Gear Move Wait Time", gearMechanism.getTimeToWaitAfterDetection()));
			gearMechanism.setAutonomous(SmartDashboard.getBoolean("Gear Automatic",gearMechanism.getAutonomous()));
			gearMechanism.setisResetToAutoAfterRemoval(SmartDashboard.getBoolean("Gear Auto After Remove",gearMechanism.getResetToAutoAfterRemoval()));
			gearMechanism.setCurrentLimit((int)SmartDashboard.getNumber("Gear Current Limit",gearMechanism.getCurrentLimit()));
			gearMechanism.setMotorSpeed(SmartDashboard.getNumber("Gear Motor Speed",gearMechanism.getMotorSpeed()));
			gearMechanism.setMotorDownSpeed(SmartDashboard.getNumber("Gear Motor Down Speed",gearMechanism.getMotorDownSpeed()));
			
		
		}
    }
    
	/**
	 * Helper function to update the dashboard with data from the robot
	 */
	private void dataToDashboard() {
		
		 SmartDashboard.putNumber("Ultrasonic Reading", (ultrasonic.getVoltage() / .2977));
		
		// Lidar Rate
	    SmartDashboard.putNumber("Lidar Read Rate", lidarReadRate);
	    
	    // Pixy Rate
		SmartDashboard.putNumber("Pixy Read Rate",pixyReadRate);
		SmartDashboard.putNumber("Ultrasonic Reading", (ultrasonic.getVoltage() / .2977896));
		
		//Automatic Drive Settings
		SmartDashboard.putNumber("Drive To Distance",drive_forward_for);
		SmartDashboard.putNumber("Turn To Angle",turn_to_angle);
		SmartDashboard.putNumber("Rotate At Rate",rotate_at_rate);
		
		if(autonomousMode != null)
		{
		    SmartDashboard.putString("Selected Autonomous Drive Mode",autonomousMode.getSelectedAutonomousDrive().toString());
		    SmartDashboard.putString("Selected Autonomous Shoot Mode",autonomousMode.getSelectedAutonomousShoot().toString());
		}
		
		if(gearSensor != null)
		{
			SmartDashboard.putNumber("Gear Detector", gearSensor.getVoltage());
		}
		
		if(gearMechanism != null)
		{
		    SmartDashboard.putString("Gear Sensor State",gearMechanism.getCurrentGearState().toString());
		    SmartDashboard.putNumber("Gear Servo Up Value", gearMechanism.getServoValueGearUp());
		    SmartDashboard.putNumber("Gear Servo Down Value", gearMechanism.getServoValueGearDown());
		    SmartDashboard.putNumber("Gear Move Wait Time", gearMechanism.getTimeToWaitAfterDetection());
			SmartDashboard.putBoolean("Gear Automatic",gearMechanism.getAutonomous());
			SmartDashboard.putBoolean("Gear Auto After Remove",gearMechanism.getResetToAutoAfterRemoval());
			
			SmartDashboard.putNumber("Gear Current Limit",gearMechanism.getCurrentLimit());
			SmartDashboard.putNumber("Gear Motor Speed",gearMechanism.getMotorSpeed());
			SmartDashboard.putNumber("Gear Motor Down Speed",gearMechanism.getMotorDownSpeed());
		
		}
		
		if(automatic_drive != null)
		{
			automatic_drive.updateDashboardPID(automatic_drive.turnPIDLoop, "Turn");
			automatic_drive.updateDashboardPID(automatic_drive.strafePIDLoop, "Strafe");
			automatic_drive.updateDashboardPID(automatic_drive.drivePIDLoop, "Drive");
			automatic_drive.updateDashboardPID(automatic_drive.rotatePIDLoop, "Rotate");
		}
		
		if(m_imu != null && m_imu.isConnected()) {
			SmartDashboard.putNumber("IMU Yaw Deviation", m_imu.getAngle());
			SmartDashboard.putNumber("IMU Rate", m_imu.getRate());
		}

		if(lidar_pwm != null)
		{
			SmartDashboard.putNumber("Lidar PWM Distance", lidar_pwm.getRangeFeet());
			SmartDashboard.putNumber("Lidar PWM Pulse Width", lidar_pwm.getPulsePeriodMs());
		}
		
		if(lidar_front != null)
		{
			//SmartDashboard.putNumber("Lidar Front Serial", lidar_front.readSerialNumber());
			SmartDashboard.putNumber("Lidar Front Distance", lidar_front.getDistance());
		}
		
		if(lidar_rear != null)
		{
			//SmartDashboard.putNumber("Lidar Rear Serial", lidar_rear.readSerialNumber());
			SmartDashboard.putNumber("Lidar Rear Distance", lidar_rear.getDistance());
		}
		
		if(pixy_front != null)
		{
			putPixyDataOnDashboard(pixy_front, "Front");
		}
		
		if(pixy_rear != null)
		{
			putPixyDataOnDashboard(pixy_rear, "Rear");
		}
	}
	
	
	/**
	 * Puts Pixy Data on the SmartDashboard
	 * @param pixycam instance
	 * @param name - Front or Rear
	 */
	private void putPixyDataOnDashboard(PixyCmu5 pixycam, String name)
	{
		double filteredAngle = 0;
		double two_tgt_constant = AutoDriver.m_front_two_tgt_area_constant;
		double one_tgt_constant = AutoDriver.m_front_one_tgt_area_constant;
		
		if(name == null)
			name = "Front";
		
		// Rear pixy smart dashboard information
		if(pixycam != null && !pixycam.getCurrentframes().isEmpty())
		{

			switch(name)
			{
				case "Front":
					filteredAngle = pixyFrontAngleFilter.pidGet();
					two_tgt_constant = AutoDriver.m_front_two_tgt_area_constant;
					one_tgt_constant = AutoDriver.m_front_one_tgt_area_constant;
					break;
				case "Rear":
					filteredAngle = pixyRearAngleFilter.pidGet();
					two_tgt_constant = AutoDriver.m_rear_two_tgt_area_constant;
					one_tgt_constant = AutoDriver.m_rear_one_tgt_area_constant;
					break;
				default:
					break;
			}
			
			SmartDashboard.putNumber("Pixy " + name + " I2C Address",pixycam.getI2CAddress());
			
			// Calculate the number of degrees from the center the current frame 
			SmartDashboard.putNumber("Pixy " + name + " Obj Detected",pixycam.getNumObjectsDetected());
			
		    SmartDashboard.putNumber("Pixy " + name + " Obj Deg from ctr",filteredAngle);
			
			try
			{
				List<PixyFrame> pixyFrames = pixycam.getCurrentframes();
				if(pixyFrames.size() >= 1)
				{
					SmartDashboard.putNumber("Pixy " + name + " Obj 1 Area",pixyFrames.get(0).area);
					
					if(pixyFrames.size() >= 2)
				    {
						SmartDashboard.putNumber("Pixy " + name + " Obj 2 Area",pixyFrames.get(1).area);
						SmartDashboard.putNumber("Pixy " + name + " Total Area",pixycam.getTotalArea(2));
					    SmartDashboard.putNumber("Pixy " + name + " Area Ratio", pixycam.getNormalizedOffset(2));

					    SmartDashboard.putNumber("Pixy " + name + " Distance",PixyCmu5.getCalcDistance( pixycam.getTotalArea(2), two_tgt_constant));
				    } else {
				    	SmartDashboard.putNumber("Pixy " + name + " Distance",PixyCmu5.getCalcDistance( pixycam.getTotalArea(1), one_tgt_constant));
				    }
				}
				
			} catch  (RuntimeException ex ) {
				System.out.println("Pixy " + name + "read error: " + ex.getMessage());
			}
		} else {
			SmartDashboard.putNumber("Pixy " + name + " Obj Detected",0);
		}
	}
    
   
    public void autonomous() { // begin autonomous
    
    	// Scan the I2c bus
    	//i2cscan.start(0.1);
    	
    	// Reset the mode timer
    	modeTimer.reset();
    	
    	// Initialize Autonomous Mode
    	autonomousMode.Start();
    	
    	// Stop any Automatic Drive functions
    	automatic_drive.stopDrive();
    	
    	// Update the dasboard
    	dataToDashboard();	
    	
    	// Enter the loop when enabled
    	while (isAutonomous() && isEnabled()) { // begin isAutonomous
    		try {
    			Thread.sleep(5);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    		// Update the dashboard
    		dataToDashboard();
    		
    		// Call Autonomous step to run the state machine
    		autonomousMode.Step();

    	} // end while isAutonomous
    	
    	autonomousMode.Stop();
    	
    } // end autonomous
    
    public void operatorControl() { // begin operator
   	
    	// shooter variables
    	boolean fire = true;
		modeTimer.reset();
    	modeTimer.start();
    	
    	boolean upright = true;
    	
    	if(!isEnabled())
    	{
    		if(automatic_drive != null)
    		{
    			automatic_drive.stopDrive();
    		}
    	}
    	
    	while (isOperatorControl() && isEnabled()) { // begin while operator & enabled
    		try {
    			Thread.sleep(5);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    		
    		dataToDashboard();
    		
    		// joysticks
    		lsyA = controller.getRawAxis(1); // finds position of the Y Axis on the left stick (Y)
    		lsxA = controller.getRawAxis(0); // finds position of the X Axis on the left stick (X)
    	rsxA = controller.getRawAxis(4); // finds position of the X Axis on the right stick (Twist)
        	
        	// joysticks
    		co_lsyA = coController.getRawAxis(1); // finds position of the Y Axis on the left stick (Y)
    		co_lsxA = coController.getRawAxis(0); // finds position of the X Axis on the left stick (X)
        	co_rsxA = coController.getRawAxis(4); // finds position of the X Axis on the right stick (Twist)
        	co_rsyA = coController.getRawAxis(5);
        	
        	// buttons
        	db_buttonLB.update(controller.getRawButton(5));
        	db_buttonRB.update(controller.getRawButton(6)); // debounced button initialization
        	db_buttonSt.update(controller.getRawButton(8)); // debounced button initialization
        	db_buttonB.update(controller.getRawButton(2));
            db_buttonA.update(controller.getRawButton(1));
            db_buttonX.update(controller.getRawButton(3));
            db_buttonY.update(controller.getRawButton(4));
            
            
            db_Co_buttonLB.update(coController.getRawButton(5));
            
            db_CobuttonRB2.update(coController.getRawButton(6));
            db_CobuttonLB2.update(coController.getRawButton(5));
            
        	//db_Co_buttonRB.update(coController.getRawButton(6)); // debounced button initialization
        	db_Co_buttonSt.update(coController.getRawButton(8)); // debounced button initialization
        	db_Co_buttonB.update(coController.getRawButton(2));
            db_Co_buttonA.update(coController.getRawButton(1));
            db_Co_buttonX.update(coController.getRawButton(3));
            db_Co_buttonY.update(coController.getRawButton(4));
            
            //db_danceStart.update(danceController.getRawButton(8));//Dancing is the Start button on the third controller
            
        	// triggers on main controller
        	zTrigL = controller.getRawAxis(2);
        	zTrigR = controller.getRawAxis(3);
        	
        	// triggers on co-controller
        	co_zTrigL = coController.getRawAxis(2);
        	co_zTrigR = coController.getRawAxis(3);
        	
        	// add a dead band to the Z axis trigger
        	zTrigL = (zTrigL < 0.2 ? 0 : -zTrigL); // backwards
        	zTrigR = (zTrigR < 0.2 ? 0 : zTrigR); // forwards
        	
        	// add a dead band to the Z axis trigger to co-controller
        	co_zTrigL = (co_zTrigL < 0.2 ? 0 : -co_zTrigL); // backwards
        	co_zTrigR = (co_zTrigR < 0.2 ? 0 : co_zTrigR); // forwards
        	
        	// total values for triggers
        	zTrigTot = -1*(zTrigR + zTrigL);
        	
        	// total values for triggers on co-controller
        	co_zTrigTot = -1*(co_zTrigR + co_zTrigL);
        	
        	/*
        	 * Hit A button
        	 * start turning until button A is pressed again || degrees are close enough to center
        	 * stop turning and allow teleoperated control, await button presses
        	 */
        	
        	/*// If B button is pressed follow target
			if(db_Co_buttonB.get())
			{
				follow = !follow;
			}*/
        	
			// If button LB is pressed, track to target

        	if(automatic_drive == null)
        	{
        		driveTrain.mecanumDrive_Cartesian(lsxA > -0.2 && lsxA < 0.2 ? 0 : -lsxA,
						  lsyA > -0.2 && lsyA < 0.2 ? 0 : -lsyA, 
				  		  rsxA > -0.2 && rsxA < 0.2 ? 0 : -rsxA, 0);
        	} else {
	    		if(areSticksMoving(rsxA, lsyA) && automatic_drive.isRunning()) {
	    			automatic_drive.stopDrive();
	    		} else if(!automatic_drive.isRunning()) {
	        		driveTrain.mecanumDrive_Cartesian(lsxA > -0.2 && lsxA < 0.2 ? 0 : -lsxA,
	        										  lsyA > -0.2 && lsyA < 0.2 ? 0 : -lsyA, 
											  		  rsxA > -0.2 && rsxA < 0.2 ? 0 : -rsxA, 0);
	        	}
        	}
        	//driveTrain.updateSmartDashboardData();
        	
    		// climber system on co-control
    		climb.arcadeDrive(co_rsxA >= 0.2 || co_rsxA <= -0.2 && co_rsyA >= 0.8 ? -1.0 : -Math.abs(co_rsyA), 0);
    		
    		// actuator function for gear delivery system
    		
    		if(db_CobuttonRB2.get() && !db_CobuttonLB2.get() && gearMechanism != null)
    		{
				gearMechanism.setAutonomous(false);
				gearMechanism.holdGearDown();
    		} else if(!db_CobuttonRB2.get() && db_CobuttonLB2.get() && gearMechanism != null){
				gearMechanism.setAutonomous(false);
				gearMechanism.raiseGear();
    		} else if(!gearMechanism.getAutonomous()) {
				gearMechanism.setAutonomous(false);
				gearMechanism.lowerGear();
    		}
    		
			/*
			//Auto Driver Test Functions
			
    		if (db_buttonA.get()) { // end if
    			if (!automatic_drive.isRunning()) {	
    				//automatic_drive.setTurnSource(microNAVX);
    				automatic_drive.turnToAngle(-1*pixy_front.getTotalDegrees(2), true);
    			} else {
    				automatic_drive.stopDrive();
    			}
    		}
    		
    		if (db_buttonB.get()) { // end if
    			if (!automatic_drive.isRunning()) {
    				//automatic_drive.setTurnSource(pixy_front);
    				automatic_drive.turnToTarget();
    			} else {
    				automatic_drive.stopDrive();
    			}
    		}
    		
    		if (db_buttonY.get()) { // end if
    			if (!automatic_drive.isRunning()) {
    				//automatic_drive.setDriveSource(lidar_front);
    				//automatic_drive.driveStraight(drive_forward_for, true, true);
    				automatic_drive.lineUpOnTarget();
    				automatic_drive.rotateAtRate(rotate_at_rate, false);
    			} else {
    				automatic_drive.stopDrive();
    			}
    		}
    		
    		if (db_buttonX.get()) { // end if
    			if (!automatic_drive.isRunning()) {
    				automatic_drive.setDriveSource(lidar_rear);
    				automatic_drive.driveToDistance(drive_forward_for,SensorDirection.REAR);
    			} else {
    				automatic_drive.stopDrive();
    			}
    		}
    		
    		if (db_buttonSt.get()) {
    			if (!automatic_drive.isRunning()) {
    				automatic_drive.turnToAngle(turn_to_angle, true);
    			} else {
    				automatic_drive.stopDrive();
    			}
    		}
    		*/
    		

    		// debug loop for testing
    		if (debug == true) {
    			// show if motors are inverted for testing purposes
	    		if (db_buttonA.get()){
					if (invert[0] == false){
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
						invert[0] = true;
					}else{
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, false);
						invert[0] = false;
					}
				}
				if (db_buttonB.get()){
					if (invert[1] == false){
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
						invert[1] = true;
					}else{
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kRearLeft, false);
						invert[1] = false;
					}
				}
				if (db_buttonX.get()){
					if (invert[2] == false){
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
						invert[2] = true;
					}else{
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kFrontRight, false);
						invert[2] = false;
					}
				}
				if (db_buttonY.get()){
					if (invert[3] == false){
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
						invert[3] = true;
					}else{
						driveTrain.setInvertedMotor(RobotDrive.MotorType.kRearRight, false);
						invert[3] = false;
					}
				}
    			
	    		showOnDash(6, "Motor FL Inverted:" + motor_FL.getInverted());
	    		showOnDash(7, "Motor FR Inverted:" + motor_FR.getInverted());
	    		showOnDash(8, "Motor RL Inverted:" + motor_RL.getInverted());
	    		showOnDash(9, "Motor RR Inverted:" + motor_RR.getInverted());
    		}
    		
    	} // end while operatorControl & enabled 

    } // end operator
    
    public void test() { // begin test
    	
    } // end test
    
    private void showOnDash(int slot, String value) {
    	String dbString = String.format("DB/String %d", slot);
    	SmartDashboard.putString(dbString, value);
    }
    
    // Convenience type overloads.
    private void showOnDash(int slot, int value) { showOnDash(slot, Integer.toString(value)); }
    private void showOnDash(int slot, long value) { showOnDash(slot, Long.toString(value)); }
    private void showOnDash(int slot, float value) { showOnDash(slot, Float.toString(value)); }
    private void showOnDash(int slot, double value) { showOnDash(slot, Double.toString(value)); }
    private void showOnDash(int slot, boolean value) { showOnDash(slot, Boolean.toString(value)); }
    
    public static boolean areSticksMoving(double leftStick, double rightStick) {
    	return !(Math.abs(leftStick) < 0.3 && Math.abs(rightStick) < 0.3);
    }
     
} // end class Robot
