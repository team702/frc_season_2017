package org.usfirst.frc.team702.robot;

import java.util.TimerTask;

import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.DigitalSource;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.Timer;

public class Shooter implements PIDSource, PIDOutput{

	public enum ShootState
	{
		STANDBY, // Does nothing
		STARTUP, // Initializes, disables driving and just focuses on shooting
		FLYWHEEL, // Phase 1 - Spins up the flywheels to get max velocity
		COLLECTOR, // Phase 2 - Spins collector to bring ball into collector (Flywheels still spinning)
		SHUTDOWN // Shuts down Flywheels and collector, and reinstates driver control
	}
	
	public static final double max_time_STANDBY = Double.POSITIVE_INFINITY;
	public static final double max_time_STARTUP = 2;
	public static final double max_time_LOCK = 5;
	public static final double max_time_FLYWHEEEL = 1;
	public static final double max_time_COLLECTOR = 0;
	public static final double max_time_SHUTDOWN = 1;
	
	
	private ShootState m_current_shoot_state = ShootState.STANDBY;
	private ShootState m_next_shoot_state = ShootState.STANDBY;

	// PID Variables (UNUSED)
	private PIDController m_shooter_pid_speed_controller;
	private double kP = .028;
	private double kI = 0.00;
	private double kD = 0.05;
	private double kF = 0.0;
	private PIDSourceType m_pid_source_type = PIDSourceType.kRate;
	
	/**********************************************************
	 *  Threading
	 **********************************************************/
    private java.util.Timer m_scheduler;
    private double m_period = 0.05;
    private int m_times_in_state = 0;
    
	// CANTalon(s) for shooting system
	private CANTalon m_motor_shooter;
	private CANTalon m_motor_collector;
	
	// Timer for the shooter
	private Timer m_shoot_timer;
	private Timer m_state_timer;
	private double m_shoot_time;
	private boolean m_shutdown_flag;
	private boolean m_startup_flag;
	
	// Encoder for speed feedback
	private Encoder m_speed_encoder;
	private double kToleranceRate = 20;
	private double m_speed_setpoint;
	

	/**
	 * ShooterTask is the private scheduler within Shooter that 
	 * automatically runs the shooter state machine.
	 */
	private class ShooterTask extends TimerTask {

        private Shooter m_shooter;

        public ShooterTask(Shooter shooter) {
          if (shooter == null) {
            throw new NullPointerException("Pixy Instance was null");
          }
          this.m_shooter = shooter;
        }

        @Override
        public void run() {
        	m_shooter.run();
        }
      }
	
	
	/**
	 * Constructor
	 * 
	 * @param CANTalon shooterMotor
	 */
	public Shooter(CANTalon shooterMotor, DigitalSource channelA, DigitalSource channelB, DigitalSource channelIdx, boolean reverse) {
		m_shooter_pid_speed_controller = new PIDController(kP,kI,kD,kF,this,this);
		m_shooter_pid_speed_controller.setInputRange(0, 5840); // RPM
		m_shooter_pid_speed_controller.setOutputRange(0, 1.0);
		m_shooter_pid_speed_controller.setAbsoluteTolerance(kToleranceRate);
		m_shooter_pid_speed_controller.setToleranceBuffer(5);
		m_shooter_pid_speed_controller.setContinuous(false);
		
		m_speed_encoder = new Encoder(channelA, channelB, channelIdx, reverse);
		m_speed_encoder.setDistancePerPulse(1/192);
		m_speed_encoder.setPIDSourceType(PIDSourceType.kRate);
		
		/* Add the PID Controller to the Test-mode dashboard, allowing manual  */
        /* tuning of the Turn Controller's P, I and D coefficients.            */
        /* Typically, only the P value needs to be modified.                   */
        LiveWindow.addActuator("DriveSystem", "ShooterController", m_shooter_pid_speed_controller);	
		
		m_motor_shooter = shooterMotor;
		
		if(m_shoot_timer == null)
		{
			m_shoot_timer = new Timer();
			m_state_timer = new Timer();
		}
		
		// Start the thread
		this.start(getPeriod());
	}
	
	public Shooter(CANTalon shooterMotor, CANTalon collectorMotor) {
		// === Removed because no encoder
		//m_shooter_pid_speed_controller = new PIDController(kP,kI,kD,kF,this,this);
		//m_shooter_pid_speed_controller.setInputRange(0, 5840); // RPM
		//m_shooter_pid_speed_controller.setOutputRange(0, 1.0);
		//m_shooter_pid_speed_controller.setAbsoluteTolerance(kToleranceRate);
		//m_shooter_pid_speed_controller.setToleranceBuffer(5);
		//m_shooter_pid_speed_controller.setContinuous(false);
		//
		//m_speed_encoder = new Encoder(channelA, channelB, channelIdx, reverse);
		//m_speed_encoder.setDistancePerPulse(1/192);
		//m_speed_encoder.setPIDSourceType(PIDSourceType.kRate);
		
		/* Add the PID Controller to the Test-mode dashboard, allowing manual  */
        /* tuning of the Turn Controller's P, I and D coefficients.            */
        /* Typically, only the P value needs to be modified.                   */
        LiveWindow.addActuator("DriveSystem", "ShooterController", m_shooter_pid_speed_controller);	
		
		m_motor_shooter = shooterMotor;
		m_motor_collector = collectorMotor;
		
		if(m_shoot_timer == null)
		{
			m_shoot_timer = new Timer();
			m_state_timer = new Timer();
		}
		
		
		// Start the thread
		this.start(getPeriod());
	}
	
	
	/**
	 * Starts the shooter
	 */
	public void startShooter()
	{
	
		m_shoot_timer.stop();
		m_shoot_timer.reset();
		m_shoot_timer.start();
		
		m_shoot_time = 0;
		
		if(m_shooter_pid_speed_controller != null)
		{
			m_shooter_pid_speed_controller.setSetpoint(m_speed_setpoint);
			m_shooter_pid_speed_controller.enable();
		}
		
		m_startup_flag = true;
	}
	
	/**
	 * Starts the shooter for a certain amount of time
	 */
	public void startShooter(double seconds)
	{
		m_shoot_timer.stop();
		m_shoot_timer.reset();
		m_shoot_timer.start();
		
		if(m_shooter_pid_speed_controller != null)
		{
			m_shooter_pid_speed_controller.setSetpoint(m_speed_setpoint);
			m_shooter_pid_speed_controller.enable();
		}
		
		m_startup_flag = true;
	}
	
	/**
	 * Stops the shooter
	 */
	public void stopShooter()
	{
		if(m_shooter_pid_speed_controller != null)
		{
			m_shooter_pid_speed_controller.disable();
			m_shooter_pid_speed_controller.reset();
		}
		
		m_shutdown_flag = true;
		m_motor_shooter.set(0);
	}
	
	/**
	 * Periodic shooter task 
	 */
	public void run()
	{
		switch(m_current_shoot_state)
		{
			case STANDBY:
				if(m_times_in_state == 0)
				{
					
				} m_times_in_state++;
				
				// Wait until a transition occurs
				if(m_startup_flag)
				{
					m_startup_flag = false;
				}
				
				break;
			case STARTUP:
				if(m_times_in_state == 0)
				{
					m_state_timer.start();
				} m_times_in_state++;
				
				// After Startup go to the flywheel state
				m_next_shoot_state = ShootState.FLYWHEEL;
				
				break;
			case FLYWHEEL:
				if(m_times_in_state == 0)
				{
					
				} m_times_in_state++;
				
				// If has spun up long enough run the collector
				m_next_shoot_state = ShootState.COLLECTOR;
				
				break;
			case COLLECTOR:
				if(m_times_in_state == 0)
				{
					
				} m_times_in_state++;
				
				// If collector has run for long enough, shut it down
				m_next_shoot_state = ShootState.SHUTDOWN;
				
				break;
			case SHUTDOWN:
				if(m_times_in_state == 0)
				{
					
				} m_times_in_state++;
				
				break;
			default:
				m_next_shoot_state = ShootState.STANDBY;
				break;
		}
		
		m_current_shoot_state = m_next_shoot_state;
	}
	
	
	/**
     * Start a thread to run the shooter
     * 
     * @param period - period in seconds to schedule update
     */
    private void start(double period)
    {
    	// Schedule the Shooter task to execute every <period> seconds
    	if(m_scheduler == null && period > 0)
    	{
    		this.setPeriod(period);
    		System.out.println("Attempting to enable Shooter at a " + Double.toString(period) + " second rate.");
    		m_scheduler = new java.util.Timer();
    		m_scheduler.schedule(new ShooterTask(this), 0L, (long) (this.getPeriod() * 1000));
    	} else {
    		System.out.println("Shooter Thread already scheduled. Stop before starting a new thread.");
    	}
    }
    
    
    /**
     * Cancel a running timer and attempt to null the variable
     */
    private void stop()
    {
    	// If the timer object is not null, cancel the scheduler
    	if(m_scheduler != null)
    	{
    		System.out.println("Attempting to disable Shooter Thread");
    		m_scheduler.cancel();
    		
    		// Set the timer to NULL to allow it to be reallocated if necessary
			m_scheduler = null;
    		
    	} else {
    		// nothing to do
    	}
    }
    
	// Getters/setters
    /**
     * Sets the frequency the read process will occur at.
     * $
     * @param period in seconds
     */
    public synchronized void setPeriod(double period)
    {
    	if(period < 0)
    	{
    		throw new IllegalArgumentException("Period must be a positive value");
    	}
    	
    	m_period = period;
    }
    
    /**
     * Gets the frequency the read process will occur at.
     * $
     * @return period in seconds
     */
    public synchronized double getPeriod()
    {
    	return this.m_period;
    }
    
    /**
	 * @return the kP
	 */
	public synchronized double getkP() {
		return kP;
	}

	/**
	 * @param kP the kP to set
	 */
	public synchronized void setkP(double kP) {
		kP = kP;
	}

	/**
	 * @return the kI
	 */
	public synchronized double getkI() {
		return kI;
	}

	/**
	 * @param kI the kI to set
	 */
	public synchronized void setkI(double kI) {
		kI = kI;
	}

	/**
	 * @return the kD
	 */
	public synchronized double getkD() {
		return kD;
	}

	/**
	 * @param kD the kD to set
	 */
	public synchronized void setkD(double kD) {
		kD = kD;
	}

	/**
	 * @return the kF
	 */
	public synchronized double getkF() {
		return kF;
	}

	/**
	 * @param kF the kF to set
	 */
	public static synchronized void setkF(double kF) {
		kF = kF;
	}

	/**
	 * @return the m_period
	 */
	public synchronized double getM_period() {
		return m_period;
	}

	/**
	 * @param m_period the m_period to set
	 */
	public synchronized void setM_period(double m_period) {
		this.m_period = m_period;
	}

	/**
	 * @return the m_speed_setpoint
	 */
	public synchronized double getM_speed_setpoint() {
		return m_speed_setpoint;
	}

	/**
	 * @param m_speed_setpoint the m_speed_setpoint to set
	 */
	public synchronized void setM_speed_setpoint(double m_speed_setpoint) {
		this.m_speed_setpoint = m_speed_setpoint;
	}
	
	// PID Interface Code

	@Override
	public void pidWrite(double output) {
		this.m_motor_shooter.set(output);
	}

	@Override
	public void setPIDSourceType(PIDSourceType pidSource) {
    	m_pid_source_type = pidSource;		
	}

	@Override
	public PIDSourceType getPIDSourceType() {
		return m_pid_source_type;
	}

	@Override
	public double pidGet() {
		return m_speed_encoder.getRate();
	}

}
