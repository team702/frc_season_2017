package org.usfirst.frc.team702.sensors;

import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.SensorBase;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.hal.FRCNetComm.tResourceType;
import edu.wpi.first.wpilibj.hal.HAL;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.livewindow.LiveWindowSendable;
import edu.wpi.first.wpilibj.tables.ITable;

public class LidarLitePWM extends SensorBase implements PIDSource, LiveWindowSendable{

	/**
	 * LidarLite PWM Rangefinder class. The lidar lite measure absolute distance based on measuring the
	 * RTT of a laser signal. This is returned on the DigitalInput pin with a pulse width of 
	 * 1 meter per mS. The DigitalInput is pulled down to ground with a 1kohm resistor so the lidar
	 * constantly polls distance. The counter class measures the period of the pulse between a 
	 * rising edge and falling edge.
	 */

	  /**
	   * The units to return when PIDGet is called.
	   */
	  public enum Unit {
	    /**
	     * Use inches for PIDGet.
	     */
	    kInches,
	    /**
	     * Use millimeters for PIDGet.
	     */
	    kMillimeters
	  }

	  // Time (sec) for the ping trigger pulse.
	  private static final double kPingTime = 50 * 1e-6;
	  private static final double kInchesPerMS = 39.3701;
	  // head of the lidar lite sensor list
	  private static LidarLitePWM m_firstSensor = null;
	  // automatic round robin mode
	  private static boolean m_automaticEnabled = false;
	  private DigitalInput m_signalChannel;
	  private boolean m_allocatedChannels;
	  private boolean m_enabled = false;
	  private Counter m_counter = null;
	  private LidarLitePWM m_nextSensor = null;
	  // task doing the round-robin automatic sensing
	  private static Thread m_task = null;
	  private Unit m_units;
	  private static int m_instances = 0;
	  protected PIDSourceType m_pidSource = PIDSourceType.kDisplacement;
	  private double m_pulse_time = 0;

	  /**
	   * Background task that goes through the list of lidar lite sensors and pings each one in turn.
	   * The counter is configured to read the timing of the returned echo pulse.
	   *
	   * <p><b>DANGER WILL ROBINSON, DANGER WILL ROBINSON:</b> This code runs as a task and assumes that
	   * none of the lidar lite sensors will change while it's running. If one does, then this will
	   * certainly break. Make sure to disable automatic mode before changing anything with the
	   * sensors!!
	   */
	  
	  private class LidarLitePWMChecker extends Thread {

	    @Override
	    public synchronized void run() {
	      LidarLitePWM lidarlite_pwm = null;
	      while (m_automaticEnabled) {
	        if (lidarlite_pwm == null) {
	          lidarlite_pwm = m_firstSensor;
	        }
	        if (lidarlite_pwm == null) {
	          return;
	        }
	        if (lidarlite_pwm.isEnabled()) {
	          // Do the ping
	          
	          
	          //lidarlite_pwm.m_pingChannel.set(false);
	        	
	        
	          //lidarlite_pwm.m_pingChannel.pulse(kPingTime);
	          
	        }
	        lidarlite_pwm = lidarlite_pwm.m_nextSensor;
	        Timer.delay(.05); // wait for ping to return
	      }
	      
	    }
	    
	  }

	  /**
	   * Initialize the lidar lite Sensor. This is the common code that initializes the lidar lite
	   * sensor given that there are two digital I/O channels allocated. If the system was running in
	   * automatic mode (round robin) when the new sensor is added, it is stopped, the sensor is added,
	   * then automatic mode is restored.
	   */
	  private synchronized void initialize() {
	    if (m_task == null) {
	      m_task = new LidarLitePWMChecker();
	    }
	    final boolean originalMode = m_automaticEnabled;
	    setAutomaticMode(false); // kill task when adding a new sensor
	    m_nextSensor = m_firstSensor;
	    m_firstSensor = this;
	    // Since the trigger is active low, set it to true

//	    m_echoChannel.setUpSourceEdge(true, false);
//	    m_echoChannel.enableInterrupts();
//	    
//	    m_pulse_time  = 0;
//	    
//	    // Register an interrupt handler
//	    m_echoChannel.requestInterrupts(new InterruptHandlerFunction<Object>() {
//
//	         @Override
//	         public void interruptFired(int interruptAssertedMask, Object param) {
//	              // Do stuff
//	         }
//	    });

	    m_counter = new Counter(m_signalChannel); // set up counter for this
	    // sensor
	    m_counter.setMaxPeriod(0.05);
	    m_counter.setSemiPeriodMode(true);
	    m_counter.reset();
	    m_enabled = true; // make it available for round robin scheduling
	    setAutomaticMode(originalMode);

	    m_instances++;
	    HAL.report(tResourceType.kResourceType_Ultrasonic, m_instances);
	    LiveWindow.addSensor("LidarLitePWM", m_signalChannel.getChannel(), this);
	  }

	  /**
	   * Create an instance of the Ultrasonic Sensor. This is designed to supchannel the Daventech SRF04
	   * and Vex ultrasonic sensors.
	   *
	   * @param echoChannel The digital input channel that receives the echo. The length of time that
	   *                    the echo is high represents the round trip time of the ping, and the
	   *                    distance.
	   * @param units       The units returned in either kInches or kMilliMeters
	   */
	  public LidarLitePWM(final int echoChannel, Unit units) {
	    m_signalChannel = new DigitalInput(echoChannel);
	    m_allocatedChannels = true;
	    m_units = units;
	    initialize();
	  }

	  /**
	   * Create an instance of the Ultrasonic Sensor. This is designed to supchannel the Daventech SRF04
	   * and Vex ultrasonic sensors. Default unit is inches.
	   *
	   * @param echoChannel The digital input channel that receives the echo. The length of time that
	   *                    the echo is high represents the round trip time of the ping, and the
	   *                    distance.
	   */
	  public LidarLitePWM(final int echoChannel) {
	    this(echoChannel, Unit.kInches);
	  }

	  /**
	   * Create an instance of an Ultrasonic Sensor from a DigitalInput for the echo channel and a
	   * DigitalOutput for the ping channel.
	   *
	   * @param pingChannel The digital output object that starts the sensor doing a ping. Requires a
	   *                    10uS pulse to start.
	   * @param echoChannel The digital input object that times the return pulse to determine the
	   *                    range.
	   * @param units       The units returned in either kInches or kMilliMeters
	   */
	  public LidarLitePWM(DigitalInput echoChannel, Unit units) {
	    if (echoChannel == null) {
	      throw new NullPointerException("Null Channel Provided");
	    }
	    m_allocatedChannels = false;
	    m_signalChannel = echoChannel;
	    m_units = units;
	    initialize();
	  }

	  /**
	   * Create an instance of an Ultrasonic Sensor from a DigitalInput for the echo channel and a
	   * DigitalOutput for the ping channel. Default unit is inches.
	   *
	   * @param echoChannel The digital input object that times the return pulse to determine the
	   *                    range.
	   */
	  public LidarLitePWM(DigitalInput echoChannel) {
	    this(echoChannel, Unit.kInches);
	  }

	  /**
	   * Destructor for the ultrasonic sensor. Delete the instance of the ultrasonic sensor by freeing
	   * the allocated digital channels. If the system was in automatic mode (round robin), then it is
	   * stopped, then started again after this sensor is removed (provided this wasn't the last
	   * sensor).
	   */
	  @Override
	  public synchronized void free() {
	    final boolean wasAutomaticMode = m_automaticEnabled;
	    setAutomaticMode(false);
	    if (m_allocatedChannels) {
	      if (m_signalChannel != null) {
	        m_signalChannel.free();
	      }
	    }

	    if (m_counter != null) {
	      m_counter.free();
	      m_counter = null;
	    }

	    m_signalChannel = null;

	    if (this == m_firstSensor) {
	      m_firstSensor = m_nextSensor;
	      if (m_firstSensor == null) {
	        setAutomaticMode(false);
	      }
	    } else {
	      for (LidarLitePWM s = m_firstSensor; s != null; s = s.m_nextSensor) {
	        if (this == s.m_nextSensor) {
	          s.m_nextSensor = s.m_nextSensor.m_nextSensor;
	          break;
	        }
	      }
	    }
	    if (m_firstSensor != null && wasAutomaticMode) {
	      setAutomaticMode(true);
	    }
	  }

	  /**
	   * Turn Automatic mode on/off. When in Automatic mode, all sensors will fire in round robin,
	   * waiting a set time between each sensor.
	   *
	   * @param enabling Set to true if round robin scheduling should start for all the ultrasonic
	   *                 sensors. This scheduling method assures that the sensors are non-interfering
	   *                 because no two sensors fire at the same time. If another scheduling algorithm
	   *                 is preferred, it can be implemented by pinging the sensors manually and waiting
	   *                 for the results to come back.
	   */
	  public void setAutomaticMode(boolean enabling) {
	    if (enabling == m_automaticEnabled) {
	      return; // ignore the case of no change
	    }
	    m_automaticEnabled = enabling;

	    if (enabling) {
	      /* Clear all the counters so no data is valid. No synchronization is
	       * needed because the background task is stopped.
	       */
	      for (LidarLitePWM u = m_firstSensor; u != null; u = u.m_nextSensor) {
	        u.m_counter.reset();
	      }

	      // Start round robin task
	      m_task.start();
	    } else {
	      // Wait for background task to stop running
	      try {
	        m_task.join();
	      } catch (InterruptedException ex) {
	        Thread.currentThread().interrupt();
	        ex.printStackTrace();
	      }

	      /* Clear all the counters (data now invalid) since automatic mode is
	       * disabled. No synchronization is needed because the background task is
	       * stopped.
	       */
	      for (LidarLitePWM u = m_firstSensor; u != null; u = u.m_nextSensor) {
	        u.m_counter.reset();
	      }
	    }
	  }

	  /**
	   * Check if there is a valid range measurement. The ranges are accumulated in a counter that will
	   * increment on each edge of the echo (return) signal. If the count is not at least 2, then the
	   * range has not yet been measured, and is invalid.
	   *
	   * @return true if the range is valid
	   */
	  public boolean isRangeValid() {
	    return m_counter.get() > 1;
	  }

	  /**
	   * Get the range in inches from the lidar lite sensor. If there is no valid value yet, i.e. at
	   * least one measurement hasn't completed, then return 0.
	   *
	   * @return double Range in inches of the target returned from the lidar lite sensor.
	   */
	  public double getRangeInches() {
	    if (isRangeValid()) {
	      return m_counter.getPeriod() * 1000 * kInchesPerMS;
	    } else {
	      return 0;
	    }
	  }
	  
	  /**
	   * Get the range in feet from the lidar lite sensor. If there is no valid value yet, i.e.
	   * at least one measurement hasn't completed, then return 0.
	   *
	   * @return double Range in feet of the target returned by the lidar lite sensor.
	   */
	  public double getRangeFeet() {
	    return getRangeInches() / 12;
	  }
	  
	  /**
	   * Get the range in feet from the lidar lite sensor. If there is no valid value yet, i.e.
	   * at least one measurement hasn't completed, then return 0.
	   *
	   * @return double Range in feet of the target returned by the lidar lite sensor.
	   */
	  public double getDistance() {
	    return getRangeFeet();
	  }

	  /**
	   * Get the range in millimeters from the lidar lite sensor. If there is no valid value yet, i.e.
	   * at least one measurement hasn't completed, then return 0.
	   *
	   * @return double Range in millimeters of the target returned by the lidar lite sensor.
	   */
	  public double getRangeMM() {
	    return getRangeInches() * 25.4;
	  }

	  public double getPulsePeriodMs() {
	    if (isRangeValid()) {
	      return m_counter.getPeriod()*1000;
	    } else {
	      return 0;
	    }
	  }
	  
	  @Override
	  public void setPIDSourceType(PIDSourceType pidSource) {
	    if (!pidSource.equals(PIDSourceType.kDisplacement)) {
	      throw new IllegalArgumentException("Only displacement PID is allowed for lidar lites.");
	    }
	    m_pidSource = pidSource;
	  }

	  @Override
	  public PIDSourceType getPIDSourceType() {
	    return m_pidSource;
	  }

	  /**
	   * Get the range in the current DistanceUnit for the PIDSource base object.
	   *
	   * @return The range in DistanceUnit
	   */
	  @Override
	  public double pidGet() {
	    switch (m_units) {
	      case kInches:
	        return getRangeInches();
	      case kMillimeters:
	        return getRangeMM();
	      default:
	        return 0.0;
	    }
	  }

	  /**
	   * Set the current DistanceUnit that should be used for the PIDSource base object.
	   *
	   * @param units The DistanceUnit that should be used.
	   */
	  public void setDistanceUnits(Unit units) {
	    m_units = units;
	  }

	  /**
	   * Get the current DistanceUnit that is used for the PIDSource base object.
	   *
	   * @return The type of DistanceUnit that is being used.
	   */
	  public Unit getDistanceUnits() {
	    return m_units;
	  }

	  /**
	   * Is the lidar lite enabled.
	   *
	   * @return true if the lidar lite is enabled
	   */
	  public boolean isEnabled() {
	    return m_enabled;
	  }

	  /**
	   * Set if the lidar lite is enabled.
	   *
	   * @param enable set to true to enable the lidar lite
	   */
	  public void setEnabled(boolean enable) {
	    m_enabled = enable;
	  }

	  /**
	   * Live Window code, only does anything if live window is activated.
	   */
	  @Override
	  public String getSmartDashboardType() {
	    return "LidarLite";
	  }

	  private ITable m_table;

	  @Override
	  public void initTable(ITable subtable) {
	    m_table = subtable;
	    updateTable();
	  }

	  @Override
	  public ITable getTable() {
	    return m_table;
	  }

	  @Override
	  public void updateTable() {
	    if (m_table != null) {
	      m_table.putNumber("Value", getRangeInches());
	    }
	  }

	  @Override
	  public void startLiveWindowMode() {
	  }

	  @Override
	  public void stopLiveWindowMode() {
	  }
	
}
