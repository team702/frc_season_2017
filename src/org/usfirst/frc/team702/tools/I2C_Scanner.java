package org.usfirst.frc.team702.tools;

import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import edu.wpi.first.wpilibj.I2C;

public class I2C_Scanner {

	private I2C m_i2cbus;
	private boolean flg_debug = true;
	public java.util.Timer m_scheduler;
	private double m_period;
	private int currentI2CAddress;
	private static final int DATA_SIZE = 50;
	private ReentrantLock m_i2c_mutex;
	private I2C.Port m_i2cport;
	private String m_detected_devices;
	
	/**
	 * PixyTask is the private scheduler within PixyCMU5 that 
	 * automatically performs I2C reads to get the frame data
	 * from the connected Pixy device.
	 */
	private class I2CTask extends TimerTask {

	    private I2C_Scanner i2c_finder;
	
	    public I2CTask(I2C_Scanner i2c_finder) {
	      if (i2c_finder == null) {
	        throw new NullPointerException("I2C Finder Instance was null");
	      }
	      this.i2c_finder = i2c_finder;
	    }
	
	    @Override
	    public void run() {
	    	i2c_finder.testI2CAddress();
	    }
	}

	public I2C_Scanner(I2C.Port I2CPort, ReentrantLock i2c_mutex)
	{
		m_i2cport = I2CPort == null ? I2C.Port.kOnboard : I2CPort;
		m_i2c_mutex = i2c_mutex == null ? new ReentrantLock() : i2c_mutex;
		m_detected_devices = "";
    	currentI2CAddress = 0;
	}

	public void testI2CAddress()
	{
		if(currentI2CAddress == 0) // First time in loop
		{
			m_detected_devices = "";
		}
		
		// Create the new port
		m_i2cbus = new I2C(m_i2cport, currentI2CAddress);
		
		if(flg_debug )
		{
			System.out.println("Trying I2C address " + currentI2CAddress);
		}
		
		// readBuffer is the raw data output from the i2c bus
    	byte [] readBuffer = new byte [DATA_SIZE];
    	byte [] zeroBuffer = new byte [DATA_SIZE];
    	
    	//Arrays.fill(readBuffer, (byte)0);
    	for(int idx = 0; idx < readBuffer.length; idx++)
    	{
    		readBuffer[idx] = (byte)0;
			zeroBuffer[idx] = (byte)0;
    	}
		
    	// Lock the mutex
		m_i2c_mutex.lock();
		try
		{
			// Attempt to read a few bytes
			m_i2cbus.readOnly(readBuffer, DATA_SIZE);
		} finally {
			m_i2c_mutex.unlock();
		}
    			
		boolean isEqual = true;
		for(int idx = 0; idx < DATA_SIZE; idx++)
		{
			if(zeroBuffer[idx] != readBuffer[idx])
			{
				isEqual = false;
			}
		}
		
		if(!isEqual) {
			for(byte i : readBuffer)
				System.out.print(i + " ");
			System.out.print("\n");
			
			System.out.println("Address found! " + currentI2CAddress);
			
			m_detected_devices += " " + Integer.toString(currentI2CAddress);
		}
		
		m_i2cbus.free();
		m_i2cbus = null;
		currentI2CAddress++;
		
		if(currentI2CAddress >= 255) //this.m_addressFound || 
		{
			this.stop();
			currentI2CAddress = 0;
		}
	}
	
	/**
	 * @return the m_detected_devices
	 */
	public synchronized String getDetectedDevices() {
		return m_detected_devices;
	}
	
	/**
     * Start a thread to read data from the I2C bus periodically
     * 
     * @param period - period in seconds to schedule update
     */
    public void start(double period)
    {
   	
    	// Schedule the Pixy task to execute every <period> seconds
    	if(m_scheduler == null && period > 0)
    	{
    		setPeriod(period);
    		System.out.println("Attempting to enable I2C Finder at a " + Double.toString(period) + " second rate.");
    		m_scheduler = new java.util.Timer();
    		m_scheduler.schedule(new I2CTask(this), 0L, (long) (this.getPeriod() * 1000));
    	} else {
    		System.out.println("I2C Finder Thread already scheduled. Stop before starting a new thread.");
    	}
    }
    
    
    /**
     * Cancel a running timer and attempt to null the variable
     */
    public void stop()
    {
    	// If the timer object is not null, cancel the scheduler
    	if(m_scheduler != null)
    	{
    		System.out.println("Devices Found: " + m_detected_devices);
    		System.out.println("Attempting to disable I2C auto polling.");
    		m_scheduler.cancel();
    		
    		// Set the timer to NULL to allow it to be reallocated if necessary
    		synchronized(this) {
    			m_scheduler = null;
    		}
    		
    	} else {
    		// nothing to do
    	}
    }

    /**
     * Sets the frequency the read process will occur at.
     * $
     * @param period in seconds
     */
    public synchronized void setPeriod(double period)
    {
    	if(period < 0)
    	{
    		throw new IllegalArgumentException("Period must be a positive value");
    	}
    	
    	m_period = period;
    }
    
    /**
     * Gets the frequency the read process will occur at.
     * $
     * @return period in seconds
     */
    public synchronized double getPeriod()
    {
    	return this.m_period;
    }
}
