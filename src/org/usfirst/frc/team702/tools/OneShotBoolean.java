package org.usfirst.frc.team702.tools;

public class OneShotBoolean extends DebouncedBoolean{
	
	public OneShotBoolean(int samples) {
		// Call the superclass constructor
		super(samples);
		
	}
	
	/**
	 * Call this method each time the value is updated
	 * 
	 * @param sample - the boolean value of the sampled input
	 * @return the debounced output
	 */
	public synchronized void update(boolean sample)
	{
		if(sample != m_previousSample)
		{
			// If a change was detected, set the timestamp
			m_timeUpdate = getTime();
			this.m_isstable = false;
		} else if(!this.m_isstable  && sample == m_previousSample && getTime() - m_timeUpdate >= m_setTime){
			// otherwise, if the sample has been the same for more than the set time, change the output
			this.set(sample);
			this.m_isstable = true;
		}
		
		m_previousSample = sample;
	}

	
	/* (non-Javadoc)
	 * @see org.usfirst.frc.team702.robot.DebouncedBoolean#get()
	 */
	public synchronized boolean get()
	{
		if(this.m_isstable && m_outputSample)
		{
			m_outputSample = false;
			return true;
		}
		
		return false;
	}


}
