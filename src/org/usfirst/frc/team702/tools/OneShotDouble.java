package org.usfirst.frc.team702.tools;

import edu.wpi.first.wpilibj.Timer;

public class OneShotDouble {

	protected double m_previousSample;
	protected double m_outputSample;
	
	protected double m_stabilityTime;
	protected double m_defaultval;
	protected boolean m_isStable;
	
	protected double m_tolerance;
	
	protected double m_timeUpdate;
	
	public OneShotDouble(double defaultval, double stabilityTime) {
		
		this.setSamples(stabilityTime);
		this.setDefaultVal(defaultval);
		this.m_timeUpdate = getTime();
		this.m_isStable = false;
		this.m_tolerance = 5;
	}
	
	
	/**
	 * Call this method each time the value is updated
	 * 
	 * @param sample - the boolean value of the sampled input
	 * @return the debounced output
	 */
	public synchronized void update(double sample)
	{
		if(sample != m_previousSample)
		{
			// If a change was detected, set the timestamp
			m_timeUpdate = getTime();
			m_isStable = false;
			
		} else if(!m_isStable && sample == m_previousSample && getTime() - m_timeUpdate >= m_stabilityTime){
			// otherwise, if the sample has been the same for more than the set time, change the output
			this.set(sample);
		}
		
		m_previousSample = sample;
		
		//return this.get();
	}
	
	protected synchronized void set(double value)
	{
		this.m_outputSample = value;
		this.m_isStable = true;
	}
	
	public synchronized double get()
	{
		double retVal = m_defaultval;
		if(m_outputSample != m_defaultval)
		{
			retVal = m_outputSample;
			m_outputSample = m_defaultval;
		}
		
		return retVal;
	}
	
	/**
	 * @param samples - number of samples needed for output to be considered true
	 */
	public synchronized void setSamples(double stabilityTime)
	{
		this.m_stabilityTime = stabilityTime;
	}
	
	public synchronized void setDefaultVal(double defaultVal)
	{
		this.m_defaultval = defaultVal;
	}
	
	private double getTime()
	{
		double timeVal = 0.0;
		try {
			timeVal = Timer.getFPGATimestamp();
		} catch (Exception e) {
			timeVal = System.currentTimeMillis();
			//e.printStackTrace();
		}
		
		return timeVal;
	}

}
