package org.usfirst.frc.team702.tools;

import edu.wpi.first.wpilibj.util.BoundaryException;
import edu.wpi.first.wpilibj.util.BaseSystemNotInitializedException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sbaron
 *
 */
public class RateLimiter {

	public enum limiterType
	{
		LINEAR,
		PERCENT,
		MOVING_AVERAGE,
		FIR_FILTER
	}
	
	public enum percentType
	{
		MAX,
		MIN,
		MAX_TOP,
		MIN_BOTTOM,
		PREVIOUS,
		DELTA
	}
	
	/**
	 *  Properties for RateLimiter
	 */
	private double  m_incRateLimit = Double.POSITIVE_INFINITY;
	private double  m_decRateLimit = Double.NEGATIVE_INFINITY;
	private boolean m_limitIncrease = false;
	private boolean m_limitDecrease = false;
	private double m_max;
	private double m_min;
	private limiterType m_limitType = limiterType.LINEAR;
	
	private int m_bufLength = 1;
    private Queue<Double> m_buf;
    private double m_bufTotal;
	
	/**
	 * Internal Variables
	 */
	private double[] m_dataArray;
	
	/**
	 * RateLimiter status flags
	 */
	private boolean m_firstInput = false;
	private boolean m_movingAverageInitialized = false;
	private boolean m_FIRInitialized = false;
	private boolean m_linearInitialized = false;
	private boolean m_percentInitialized = false;
	
	/**
	 * Properties for Timer process
	 */
	private double m_period;
	
	/**
	 * @param increaseRateLimit - The rate in units/period that is applied when the signal is increasing
	 * @param decreaseRateLimit - The rate in units/period that is applied when the signal is decreasing
	 * @param minValue - The lower limit that the signal can be set to
	 * @param maxValue - The upper limit that the signal can be set to
	 */
	public RateLimiter(double increaseRateLimit, double decreaseRateLimit, double minValue, double maxValue) {
		
		this.setDecreaseRate(decreaseRateLimit);
		this.setIncreaseRate(increaseRateLimit);
		this.setMinMax(minValue, maxValue);
	}
	
	/**
	 * @param minValue - The lower limit that the signal can be set to
	 * @param maxValue - The upper limit that the signal can be set to
	 */
	public RateLimiter(double minValue, double maxValue) {
		this.setMinMax(minValue, maxValue);
	}
	
	/**
	 * @param minValue - The lower limit that the signal can be set to
	 * @param maxValue - The upper limit that the signal can be set to
	 */
	public RateLimiter() {
		this.setMinMax(-1.0,1.0);
	}
	
	/**
	 * @param increaseRateLimit - The rate in units/period that is applied when the signal is increasing
	 * @param decreaseRateLimit - The rate in units/period that is applied when the signal is decreasing
	 * @param minValue - The lower limit that the signal can be set to
	 * @param maxValue - The upper limit that the signal can be set to
	 * @param period - The period that the filter is updated
	 */
	public RateLimiter(double increaseRateLimit, double decreaseRateLimit, double minValue, double maxValue, double period) {
		
		this.setDecreaseRate(decreaseRateLimit);
		this.setIncreaseRate(increaseRateLimit);
		this.setMinMax(minValue, maxValue);
		this.setPeriod(period);
	}

	/**
	 * @param rateLimit - The rate in units/period that is applied when the signal is changing
	 * @param minValue - The lower limit that the signal can be set to
	 * @param maxValue - The upper limit that the signal can be set to
	 */
	public RateLimiter(double rateLimit, double minValue, double maxValue) {
		
		this.setDecreaseRate(rateLimit);
		this.setIncreaseRate(rateLimit);
		this.setMinMax(minValue, maxValue);
	}
	
	/**
	 * Resets the rate limiter to zero
	 */
	public void reset()
	{
		m_buf.clear();
	}
		
	
	/**
	 * The wrapper for all limiter algorithms. Takes the current input sample and 
	 * timestamp and returns a rate limited value based on the selected filter.
	 * 
	 * @param inputSample
	 * @param timestamp
	 * @return
	 */
	private double f_limiter(double inputSample, double timestamp)
	{
		// Call the limiter depending on the selected algorithm
		switch(m_limitType){
			case LINEAR:
				if(m_linearInitialized)
				{
					return this.linearLimiter(inputSample, timestamp);
				} else {
					throw new BaseSystemNotInitializedException("RateLimiter::limit - Linear limiter uninitialized!");
				}
				
			case FIR_FILTER:
				if(m_FIRInitialized)
				{
					
				} else {
					throw new BaseSystemNotInitializedException("RateLimiter::limit - FIR Filter limiter uninitialized!");
				}
				
			case MOVING_AVERAGE:
				if(m_movingAverageInitialized)
				{
					return this.movingAverageLimiter(inputSample);
				} else {
					throw new BaseSystemNotInitializedException( "RateLimiter::limit - Moving Average limiter uninitialized!");
				}
				
			case PERCENT:
				if(m_percentInitialized)
				{
					
				} else {
					throw new BaseSystemNotInitializedException( "RateLimiter::limit - Percent limiter uninitialized!");
				}
				
			default:
		}

		return 0.0;
	}
	
	
	/**
	 * An overloaded wrapper for the f_limiter method which takes
	 * the timestamp as a discrete value.
	 * 
	 * @param inputSample - The next sample
	 * @param timestamp - The timestamp in milliseconds
	 * @return - The limited value of the result
	 */
	public double limit(double inputSample, double timestamp)
	{
		return f_limiter(inputSample, timestamp);	
	}
	
	/**
	 * public double limit(double inputSample)
	 * An overloaded wrapper for the f_limiter method which 
	 * uses the System.currentTimeMillis() for the timestamp
	 * 
	 * @param inputSample - The next sample
	 * @return - The limited value of the result
	 */
	public double limit(double inputSample)
	{
		double timestamp = System.currentTimeMillis();
		
		return f_limiter(inputSample, timestamp);
	}
	
	
	public double limit(boolean inputSample)
	{		
		return f_limiter(inputSample ? 1:0, System.currentTimeMillis());
	}
	
	/**
	 * Must be called once prior to calling limit().
	 * Initializes any values required for the filter
	 */
	public void initLinearLimiter()
	{
		m_limitType = limiterType.LINEAR;
		m_linearInitialized = true;
	}
	
	/**
	 * Must be called once prior to calling limit().
	 * Initializes any values required for the filter
	 */
	public void initMovingAverageLimiter(int bufSize)
	{
		m_limitType = limiterType.MOVING_AVERAGE;
		m_buf = new LinkedList<Double>();
		m_bufLength = bufSize;
		m_bufTotal = 0;
		m_movingAverageInitialized= true;
	}
	
	
	private double movingAverageLimiter(double inputSample)
	{
		double outputValue = 0;
		
	
		m_buf.add(inputSample);
        m_bufTotal += inputSample;
        // Remove old elements when the buffer is full.
        if (m_buf.size() > m_bufLength) {
          m_bufTotal -= m_buf.remove();
        }
        
        outputValue = m_bufTotal/m_buf.size();
		
		
		return outputValue;
	}
	
	/**
	 * Private class that implements the linearLimiter filter
	 * 
	 * @param inputSample
	 * @param timestamp
	 * @return
	 */
	private double linearLimiter(double inputSample, double timestamp)
	{
		double outputSample = inputSample;
		
		// If the data array is empty it is the first time this method
		// is being called, return the input directly to the output
		if(m_dataArray == null)
		{
			m_dataArray = new double[1];
			m_dataArray[0] = inputSample;
		
		} else {
			
			if(inputSample > m_dataArray[0])
			{
				// If the input is greater than the previous element
				
				if(m_limitIncrease && (inputSample - m_dataArray[0]) > m_incRateLimit)
				{
					outputSample = m_dataArray[0] + m_incRateLimit;
				}
				
			} else if(inputSample < m_dataArray[0]) {
				// If the input is less than the previous element
				
				if(m_limitDecrease && (m_dataArray[0] - inputSample) > m_decRateLimit)
				{
					outputSample = m_dataArray[0] - m_decRateLimit;
				}
			}
		}
		
		outputSample = Math.min(m_max,Math.max(outputSample,m_min));
		m_dataArray[0] = outputSample;
		return outputSample;
	}
	
	
	/**
	 * Initialized the rate limiter using the percent algorithm.
	 * 
	 * TODO: Implement initPercentLimiter
	 */
	public void initPercentLimiter()
	{
		m_limitType = limiterType.PERCENT;
		m_percentInitialized = false;
	}
	
	
	/**
	 * Private class that implements the percentLimiter filter
	 * 
	 * TODO: Implement percentLimiter filter
	 * 
	 * @param inputSample
	 * @param timestamp
	 * @return
	 */
	private double percentLimiter(double inputSample, double timestamp)
	{
		double outputSample = inputSample;
		
		// If the data array is empty it is the first time this method
		// is being called, return the input directly to the output
		if(m_dataArray == null)
		{
			m_dataArray = new double[1];
			m_dataArray[1] = inputSample;
		
		} else {
			
			if(inputSample > m_dataArray[0])
			{
				// If the input is greater than the previous element
				
				if(m_limitIncrease && (inputSample - m_dataArray[0]) > m_incRateLimit)
				{
					outputSample = m_dataArray[0] + m_incRateLimit;
				}
				
			} else if(inputSample < m_dataArray[0]) {
				// If the input is less than the previous element
				
				if(m_limitDecrease && (m_dataArray[0] - inputSample) > m_decRateLimit)
				{
					outputSample = m_dataArray[0] - m_decRateLimit;
				}
			}
		}
		
		outputSample = Math.min(m_max,Math.max(outputSample,m_min));
		m_dataArray[0] = outputSample;
		return outputSample;
	}
	
	
	/***************************************************************
	 *  
	 *  Setters/Getters
	 *  
	 ***************************************************************/
	
	
	/**
	 * Sets the min/max value of the output signal and performs error checking
	 * 
	 * @param min - The minimum value of the output
	 * @param max - The maximum value of the output
	 */
	public void setMinMax(double min, double max)
	{
		if(max < min)
		{
			throw new BoundaryException("RateLimiter::setMinMax - Max value cannot be less than min!");
		}
		
		this.m_min = min;
		this.m_max = max;
	}
	 
	/**
	 * Sets the rate limit for signals that are increasing
	 * 
	 * @param increaseRate - The rate of increase per period. In order to remove
	 *  the rate limit on increase, set the value of the argument to Double.POSITIVE_INFINITY.
	 */
	public void setIncreaseRate(double increaseRate)
	{
		if(increaseRate != Double.POSITIVE_INFINITY)
		{
			m_incRateLimit = increaseRate;
			m_limitIncrease = true;
		} else {
			m_incRateLimit = 0.0;
			m_limitIncrease = false;
		}
	}
	
	/**
	 * Sets the rate limit for signals that are decreasing
	 * 
	 * @param decreaseRate - The rate of increase per period. In order to remove
	 *  the rate limit on increase, set the value of the argument to Double.NEGATIVE_INFINITY.
	 */
	public void setDecreaseRate(double decreaseRate)
	{
		if(decreaseRate != Double.NEGATIVE_INFINITY)
		{
			m_decRateLimit = decreaseRate;
			m_limitDecrease = true;
		} else {
			m_decRateLimit = 0.0;
			m_limitDecrease = false;
		}
	}
	
	/**
	 * Sets the period which the limiter uses
	 * 
	 * @param period - The period in seconds to update the rate limiter
	 */
	public void setPeriod(double period)
	{
		m_period = period;
	}
	
	

}
